
.text

    B main // branch to main loop


func: // set up func

    STR FP, [SP, #16]
    ADD FP, SP, #20
    STR LR, [FP, #-8]
    push {R8, R9, R10}
    vpush {S30}
    LDR R0, =string_3
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    LDR R0, =string_4
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    LDR R10, [FP, #-16] // load R10 : func.int
    MOV R0, R10
    LDR R1, =integer_format
    push {LR}
    push {R0}
    push {R1} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    LDR R0, =string_5
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    VLDR.f32 S30, [FP, #-20] // load S30 : func.flt
    VMOV.f32 S0, S30
    fcvtds D0, S0
    vmov R2, R3, D0
    LDR R0, =float_format
    push {LR}
    push {R3}
    push {R2}
    push {R1}
    push {R0} // saved all temp registers
    ADD R0, SP, #16
    AND R0, R0, #7
    CMP R0, #0
    BEQ stack_align_0
    pop {R0}
    pop {R1}
    pop {R2}
    pop {R3} // reloaded temp registers in correct order
    SUB SP, #4
    BL printf
    ADD SP, #4
    B end_align_0

stack_align_0: // resume {'func.int': 'R10'}
    pop {R0}
    pop {R1}
    pop {R2}
    pop {R3} // reloaded temp registers in correct order
    BL printf

end_align_0:
    pop {LR}
    LDR R0, =string_6
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    LDR R9, [FP, #-12] // load R9 : func.str
    MOV R0, R9
    LDR R1, =string_format
    push {LR}
    push {R0}
    push {R1} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    LDR R0, =string_7
    MOV R8, R0 // func.func : R8
    STR R8, [FP, #-0] // store func.func : R8
    vpop {S30}
    pop {R8, R9, R10}
    MOV SP, FP
    LDR LR, [FP, #-8] // load LR : func..lr
    LDR R0, [FP, #-4] // load R0 : func..fp
    MOV FP, R0 // resume {} // resume {}
    BX LR // return from function


.global main
.balign 4
main:
    push {FP, LR} // save FP and LR
    MOV FP, SP
    SUB SP, #12 // make room for 3 variables
    LDR R0, =string_8
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    SUB R0, FP, #4
    MOV R1, R0
    push {LR}
    push {R1} // saved all temp registers
    pop {R0} // reloaded temp registers in correct order
    BL __read_string
    pop {LR}
    MOV R10, R0 // stored string
    LDR R0, =string_9
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    SUB R0, FP, #0
    MOV R1, R0
    LDR R2, =integer_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL __isoc99_scanf
    pop {LR}
    LDR R9, [FP, #-0]
    LDR R0, =string_10
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    SUB R0, FP, #8
    MOV R1, R0
    LDR R2, =float_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL __isoc99_scanf
    pop {LR}
    VLDR.f32 S30, [FP, #-8]
    push {LR}
    SUB SP, #12
    MOV R0, R10
    push {R0}
    MOV R1, R9
    push {R1}
    VMOV.f32 S0, S30
    vpush {S0} // saved all temp registers
    BL func
    pop {R0}
    pop {LR}
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    LDR R0, =0 // return from main
    MOV SP, FP
    pop {FP, LR}
    BX LR
.data

.balign 4
string_format:
    .asciz "%s"

.balign 4
integer_format:
    .asciz "%d"

.balign 4
float_format:
    .asciz "%f"

.balign 4
string_3:
    .asciz "\npassed to function:"

.balign 4
string_4:
    .asciz "\n - int: "

.balign 4
string_5:
    .asciz "\n - float: "

.balign 4
string_6:
    .asciz "\n - string: "

.balign 4
string_7:
    .asciz "\nthis string was returned from the function!\n"

.balign 4
string_8:
    .asciz "input a string: "

.balign 4
string_9:
    .asciz "input an integer: "

.balign 4
string_10:
    .asciz "input a float: "

.balign 4
__read_string:
    // This is the compiled gcc output to a read_string function modified from:
    //  https://www.mkyong.com/c/how-to-handle-unknow-size-user-input-in-c/
    push	{r7, lr}
	sub	sp, sp, #24
	add	r7, sp, #0
	str	r0, [r7, #4]
	movs	r3, #128
	str	r3, [r7, #12]
	movs	r3, #0
	str	r3, [r7, #20]
	ldr	r0, [r7, #12]
	bl	malloc
	mov	r3, r0
	str	r3, [r7, #4]
	ldr	r3, [r7, #12]
	str	r3, [r7, #20]
	ldr	r3, [r7, #4]
	cmp	r3, #0
	beq	__read_string.L2
	mov	r3, #-1
	str	r3, [r7, #8]
	movs	r3, #0
	str	r3, [r7, #16]
	b	__read_string.L3
__read_string.L5:
	ldr	r3, [r7, #16]
	adds	r2, r3, #1
	str	r2, [r7, #16]
	ldr	r2, [r7, #4]
	add	r3, r3, r2
	ldr	r2, [r7, #8]
	uxtb	r2, r2
	strb	r2, [r3]
	ldr	r2, [r7, #16]
	ldr	r3, [r7, #20]
	cmp	r2, r3
	bne	__read_string.L3
	ldr	r2, [r7, #16]
	ldr	r3, [r7, #12]
	add	r3, r3, r2
	str	r3, [r7, #20]
	ldr	r0, [r7, #4]
	ldr	r1, [r7, #20]
	bl	realloc
	str	r0, [r7, #4]
__read_string.L3:
	bl	getchar
	str	r0, [r7, #8]
	ldr	r3, [r7, #8]
	cmp	r3, #10
	beq	__read_string.L4
	ldr	r3, [r7, #8]
	cmp	r3, #-1
	bne	__read_string.L5
__read_string.L4:
	ldr	r2, [r7, #4]
	ldr	r3, [r7, #16]
	add	r3, r3, r2
	movs	r2, #0
	strb	r2, [r3]
__read_string.L2:
	ldr	r3, [r7, #4]
	mov	r0, r3
	adds	r7, r7, #24
	mov	sp, r7
	@ sp needed
	pop	{r7, pc}
