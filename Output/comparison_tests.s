
.text

    B main // branch to main loop


gte: // set up gte

    STR FP, [SP, #12]
    ADD FP, SP, #16
    STR LR, [FP, #-8]
    push {R10}
    vpush {S29, S30}
    VLDR.f32 S30, [FP, #-12] // load S30 : gte.v1
    VMOV.f32 S0, S30
    fcvtds D0, S0
    vmov R2, R3, D0
    LDR R0, =float_format
    push {LR}
    push {R3}
    push {R2}
    push {R1}
    push {R0} // saved all temp registers
    ADD R0, SP, #16
    AND R0, R0, #7
    CMP R0, #0
    BEQ stack_align_0
    pop {R0}
    pop {R1}
    pop {R2}
    pop {R3} // reloaded temp registers in correct order
    SUB SP, #4
    BL printf
    ADD SP, #4
    B end_align_0

stack_align_0: // resume {}
    pop {R0}
    pop {R1}
    pop {R2}
    pop {R3} // reloaded temp registers in correct order
    BL printf

end_align_0:
    pop {LR}
    LDR R0, =string_3
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    VLDR.f32 S29, [FP, #-16] // load S29 : gte.v2
    VMOV.f32 S2, S29
    fcvtds D0, S2
    vmov R2, R3, D0
    LDR R0, =float_format
    push {LR}
    push {R3}
    push {R2}
    push {R1}
    push {R0} // saved all temp registers
    ADD R0, SP, #16
    AND R0, R0, #7
    CMP R0, #0
    BEQ stack_align_1
    pop {R0}
    pop {R1}
    pop {R2}
    pop {R3} // reloaded temp registers in correct order
    SUB SP, #4
    BL printf
    ADD SP, #4
    B end_align_1

stack_align_1: // resume {}
    pop {R0}
    pop {R1}
    pop {R2}
    pop {R3} // reloaded temp registers in correct order
    BL printf

end_align_1:
    pop {LR}

start_if_0:
    VCMP.f32 S30, S29
    fmstat
    BLT else_0
    LDR R0, =string_4
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR} // resume {}
    B endif_0

else_0:
    LDR R0, =string_5
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}

endif_0:
    LDR R10, =0
    STR R10, [FP, #-0] // store gte.gte : R10
    vpop {S29, S30}
    pop {R10}
    MOV SP, FP
    LDR LR, [FP, #-8] // load LR : gte..lr
    LDR R0, [FP, #-4] // load R0 : gte..fp
    MOV FP, R0 // resume {} // resume {}
    BX LR // return from function

lte: // set up lte

    STR FP, [SP, #12]
    ADD FP, SP, #16
    STR LR, [FP, #-8]
    push {R10}
    vpush {S29, S30}
    VLDR.f32 S30, [FP, #-12] // load S30 : lte.v1
    VMOV.f32 S0, S30
    fcvtds D0, S0
    vmov R2, R3, D0
    LDR R0, =float_format
    push {LR}
    push {R3}
    push {R2}
    push {R1}
    push {R0} // saved all temp registers
    ADD R0, SP, #16
    AND R0, R0, #7
    CMP R0, #0
    BEQ stack_align_2
    pop {R0}
    pop {R1}
    pop {R2}
    pop {R3} // reloaded temp registers in correct order
    SUB SP, #4
    BL printf
    ADD SP, #4
    B end_align_2

stack_align_2: // resume {}
    pop {R0}
    pop {R1}
    pop {R2}
    pop {R3} // reloaded temp registers in correct order
    BL printf

end_align_2:
    pop {LR}
    LDR R0, =string_6
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    VLDR.f32 S29, [FP, #-16] // load S29 : lte.v2
    VMOV.f32 S2, S29
    fcvtds D0, S2
    vmov R2, R3, D0
    LDR R0, =float_format
    push {LR}
    push {R3}
    push {R2}
    push {R1}
    push {R0} // saved all temp registers
    ADD R0, SP, #16
    AND R0, R0, #7
    CMP R0, #0
    BEQ stack_align_3
    pop {R0}
    pop {R1}
    pop {R2}
    pop {R3} // reloaded temp registers in correct order
    SUB SP, #4
    BL printf
    ADD SP, #4
    B end_align_3

stack_align_3: // resume {}
    pop {R0}
    pop {R1}
    pop {R2}
    pop {R3} // reloaded temp registers in correct order
    BL printf

end_align_3:
    pop {LR}

start_if_1:
    VCMP.f32 S29, S30
    fmstat
    BLT else_1
    LDR R0, =string_4
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR} // resume {}
    B endif_1

else_1:
    LDR R0, =string_5
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}

endif_1:
    LDR R10, =0
    STR R10, [FP, #-0] // store lte.lte : R10
    vpop {S29, S30}
    pop {R10}
    MOV SP, FP
    LDR LR, [FP, #-8] // load LR : lte..lr
    LDR R0, [FP, #-4] // load R0 : lte..fp
    MOV FP, R0 // resume {} // resume {}
    BX LR // return from function

lt: // set up lt

    STR FP, [SP, #12]
    ADD FP, SP, #16
    STR LR, [FP, #-8]
    push {R10}
    vpush {S29, S30}
    VLDR.f32 S30, [FP, #-12] // load S30 : lt.v1
    VMOV.f32 S0, S30
    fcvtds D0, S0
    vmov R2, R3, D0
    LDR R0, =float_format
    push {LR}
    push {R3}
    push {R2}
    push {R1}
    push {R0} // saved all temp registers
    ADD R0, SP, #16
    AND R0, R0, #7
    CMP R0, #0
    BEQ stack_align_4
    pop {R0}
    pop {R1}
    pop {R2}
    pop {R3} // reloaded temp registers in correct order
    SUB SP, #4
    BL printf
    ADD SP, #4
    B end_align_4

stack_align_4: // resume {}
    pop {R0}
    pop {R1}
    pop {R2}
    pop {R3} // reloaded temp registers in correct order
    BL printf

end_align_4:
    pop {LR}
    LDR R0, =string_7
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    VLDR.f32 S29, [FP, #-16] // load S29 : lt.v2
    VMOV.f32 S2, S29
    fcvtds D0, S2
    vmov R2, R3, D0
    LDR R0, =float_format
    push {LR}
    push {R3}
    push {R2}
    push {R1}
    push {R0} // saved all temp registers
    ADD R0, SP, #16
    AND R0, R0, #7
    CMP R0, #0
    BEQ stack_align_5
    pop {R0}
    pop {R1}
    pop {R2}
    pop {R3} // reloaded temp registers in correct order
    SUB SP, #4
    BL printf
    ADD SP, #4
    B end_align_5

stack_align_5: // resume {}
    pop {R0}
    pop {R1}
    pop {R2}
    pop {R3} // reloaded temp registers in correct order
    BL printf

end_align_5:
    pop {LR}

start_if_2:
    VCMP.f32 S30, S29
    fmstat
    BGE else_2
    LDR R0, =string_4
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR} // resume {}
    B endif_2

else_2:
    LDR R0, =string_5
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}

endif_2:
    LDR R10, =0
    STR R10, [FP, #-0] // store lt.lt : R10
    vpop {S29, S30}
    pop {R10}
    MOV SP, FP
    LDR LR, [FP, #-8] // load LR : lt..lr
    LDR R0, [FP, #-4] // load R0 : lt..fp
    MOV FP, R0 // resume {} // resume {}
    BX LR // return from function

gt: // set up gt

    STR FP, [SP, #12]
    ADD FP, SP, #16
    STR LR, [FP, #-8]
    push {R10}
    vpush {S29, S30}
    VLDR.f32 S30, [FP, #-12] // load S30 : gt.v1
    VMOV.f32 S0, S30
    fcvtds D0, S0
    vmov R2, R3, D0
    LDR R0, =float_format
    push {LR}
    push {R3}
    push {R2}
    push {R1}
    push {R0} // saved all temp registers
    ADD R0, SP, #16
    AND R0, R0, #7
    CMP R0, #0
    BEQ stack_align_6
    pop {R0}
    pop {R1}
    pop {R2}
    pop {R3} // reloaded temp registers in correct order
    SUB SP, #4
    BL printf
    ADD SP, #4
    B end_align_6

stack_align_6: // resume {}
    pop {R0}
    pop {R1}
    pop {R2}
    pop {R3} // reloaded temp registers in correct order
    BL printf

end_align_6:
    pop {LR}
    LDR R0, =string_8
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    VLDR.f32 S29, [FP, #-16] // load S29 : gt.v2
    VMOV.f32 S2, S29
    fcvtds D0, S2
    vmov R2, R3, D0
    LDR R0, =float_format
    push {LR}
    push {R3}
    push {R2}
    push {R1}
    push {R0} // saved all temp registers
    ADD R0, SP, #16
    AND R0, R0, #7
    CMP R0, #0
    BEQ stack_align_7
    pop {R0}
    pop {R1}
    pop {R2}
    pop {R3} // reloaded temp registers in correct order
    SUB SP, #4
    BL printf
    ADD SP, #4
    B end_align_7

stack_align_7: // resume {}
    pop {R0}
    pop {R1}
    pop {R2}
    pop {R3} // reloaded temp registers in correct order
    BL printf

end_align_7:
    pop {LR}

start_if_3:
    VCMP.f32 S29, S30
    fmstat
    BGE else_3
    LDR R0, =string_4
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR} // resume {}
    B endif_3

else_3:
    LDR R0, =string_5
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}

endif_3:
    LDR R10, =0
    STR R10, [FP, #-0] // store gt.gt : R10
    vpop {S29, S30}
    pop {R10}
    MOV SP, FP
    LDR LR, [FP, #-8] // load LR : gt..lr
    LDR R0, [FP, #-4] // load R0 : gt..fp
    MOV FP, R0 // resume {} // resume {}
    BX LR // return from function


.global main
.balign 4
main:
    push {FP, LR} // save FP and LR
    MOV FP, SP
    SUB SP, #8 // make room for 2 variables
    movw R0, 0b1001100110011010
    movt R0, 0b0100000010101001
    vmov S0, R0
    VMOV.f32 S30, S0 // main.o : S30
    push {LR}
    SUB SP, #12
    VMOV.f32 S1, S30
    vpush {S1}
    movw R0, 0b1001100110011010
    movt R0, 0b0011111110011001
    vmov S2, R0
    vpush {S2} // saved all temp registers
    BL gte
    pop {R0}
    pop {LR}
    MOV R10, R0 // main.thing : R10
    push {LR}
    SUB SP, #12
    VMOV.f32 S3, S30
    vpush {S3}
    VMOV.f32 S4, S30
    vpush {S4} // saved all temp registers
    BL gte
    pop {R0}
    pop {LR}
    MOV R10, R0 // main.thing : R10
    push {LR}
    SUB SP, #12
    VMOV.f32 S5, S30
    vpush {S5}
    movw R0, 0b1100110011001101
    movt R0, 0b0100000011101100
    vmov S6, R0
    vpush {S6} // saved all temp registers
    BL gte
    pop {R0}
    pop {LR}
    MOV R10, R0 // main.thing : R10
    push {LR}
    SUB SP, #12
    VMOV.f32 S7, S30
    vpush {S7}
    movw R0, 0b1001100110011010
    movt R0, 0b0011111110011001
    vmov S0, R0
    vpush {S0} // saved all temp registers
    BL lte
    pop {R0}
    pop {LR}
    MOV R10, R0 // main.thing : R10
    push {LR}
    SUB SP, #12
    VMOV.f32 S1, S30
    vpush {S1}
    VMOV.f32 S2, S30
    vpush {S2} // saved all temp registers
    BL lte
    pop {R0}
    pop {LR}
    MOV R10, R0 // main.thing : R10
    push {LR}
    SUB SP, #12
    VMOV.f32 S3, S30
    vpush {S3}
    movw R0, 0b1100110011001101
    movt R0, 0b0100000011101100
    vmov S4, R0
    vpush {S4} // saved all temp registers
    BL lte
    pop {R0}
    pop {LR}
    MOV R10, R0 // main.thing : R10
    push {LR}
    SUB SP, #12
    VMOV.f32 S5, S30
    vpush {S5}
    movw R0, 0b1001100110011010
    movt R0, 0b0011111110011001
    vmov S6, R0
    vpush {S6} // saved all temp registers
    BL lt
    pop {R0}
    pop {LR}
    MOV R10, R0 // main.thing : R10
    push {LR}
    SUB SP, #12
    VMOV.f32 S7, S30
    vpush {S7}
    VMOV.f32 S0, S30
    vpush {S0} // saved all temp registers
    BL lt
    pop {R0}
    pop {LR}
    MOV R10, R0 // main.thing : R10
    push {LR}
    SUB SP, #12
    VMOV.f32 S1, S30
    vpush {S1}
    movw R0, 0b1100110011001101
    movt R0, 0b0100000011101100
    vmov S2, R0
    vpush {S2} // saved all temp registers
    BL lt
    pop {R0}
    pop {LR}
    MOV R10, R0 // main.thing : R10
    push {LR}
    SUB SP, #12
    VMOV.f32 S3, S30
    vpush {S3}
    movw R0, 0b1001100110011010
    movt R0, 0b0011111110011001
    vmov S4, R0
    vpush {S4} // saved all temp registers
    BL gt
    pop {R0}
    pop {LR}
    MOV R10, R0 // main.thing : R10
    push {LR}
    SUB SP, #12
    VMOV.f32 S5, S30
    vpush {S5}
    VMOV.f32 S6, S30
    vpush {S6} // saved all temp registers
    BL gt
    pop {R0}
    pop {LR}
    MOV R10, R0 // main.thing : R10
    push {LR}
    SUB SP, #12
    VMOV.f32 S7, S30
    vpush {S7}
    movw R0, 0b1100110011001101
    movt R0, 0b0100000011101100
    vmov S0, R0
    vpush {S0} // saved all temp registers
    BL gt
    pop {R0}
    pop {LR}
    MOV R10, R0 // main.thing : R10
    LDR R0, =0 // return from main
    MOV SP, FP
    pop {FP, LR}
    BX LR
.data

.balign 4
string_format:
    .asciz "%s"

.balign 4
integer_format:
    .asciz "%d"

.balign 4
float_format:
    .asciz "%f"

.balign 4
string_3:
    .asciz " >= "

.balign 4
string_4:
    .asciz " true\n"

.balign 4
string_5:
    .asciz " false\n"

.balign 4
string_6:
    .asciz " <= "

.balign 4
string_7:
    .asciz " <  "

.balign 4
string_8:
    .asciz " >  "
