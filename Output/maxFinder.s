
.text

    B main // branch to main loop


max_num: // set up max_num

    STR FP, [SP, #12]
    ADD FP, SP, #16
    STR LR, [FP, #-8]
    vpush {S28, S29, S30}

start_if_0:
    VLDR.f32 S30, [FP, #-12] // load S30 : max_num.num1
    VLDR.f32 S29, [FP, #-16] // load S29 : max_num.num2
    VCMP.f32 S30, S29
    fmstat
    BLT else_0
    VMOV.f32 S28, S30 // max_num.max_num : S28 // resume {}
    B endif_0

else_0:
    VMOV.f32 S28, S29 // max_num.max_num : S28

endif_0:
    VSTR.f32 S28, [FP, #-0] // store max_num.max_num : S28
    vpop {S28, S29, S30}
    MOV SP, FP
    LDR LR, [FP, #-8] // load LR : max_num..lr
    LDR R0, [FP, #-4] // load R0 : max_num..fp
    MOV FP, R0 // resume {} // resume {}
    BX LR // return from function


.global main
.balign 4
main:
    push {FP, LR} // save FP and LR
    MOV FP, SP
    SUB SP, #8 // make room for 2 variables
    LDR R0, =string_3
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    LDR R0, =string_4
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    SUB R0, FP, #0
    MOV R1, R0
    LDR R2, =float_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL __isoc99_scanf
    pop {LR}
    VLDR.f32 S30, [FP, #-0]
    LDR R0, =string_5
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    SUB R0, FP, #4
    MOV R1, R0
    LDR R2, =float_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL __isoc99_scanf
    pop {LR}
    VLDR.f32 S29, [FP, #-4]
    push {LR}
    SUB SP, #12
    VMOV.f32 S0, S30
    vpush {S0}
    VMOV.f32 S1, S29
    vpush {S1} // saved all temp registers
    BL max_num
    vpop {S2}
    pop {LR}
    VMOV.f32 S3, S2
    fcvtds D0, S3
    vmov R2, R3, D0
    LDR R0, =float_format
    push {LR}
    push {R3}
    push {R2}
    push {R1}
    push {R0} // saved all temp registers
    ADD R0, SP, #16
    AND R0, R0, #7
    CMP R0, #0
    BEQ stack_align_0
    pop {R0}
    pop {R1}
    pop {R2}
    pop {R3} // reloaded temp registers in correct order
    SUB SP, #4
    BL printf
    ADD SP, #4
    B end_align_0

stack_align_0: // resume {}
    pop {R0}
    pop {R1}
    pop {R2}
    pop {R3} // reloaded temp registers in correct order
    BL printf

end_align_0:
    pop {LR}
    LDR R0, =string_6
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    LDR R0, =0 // return from main
    MOV SP, FP
    pop {FP, LR}
    BX LR
.data

.balign 4
string_format:
    .asciz "%s"

.balign 4
integer_format:
    .asciz "%d"

.balign 4
float_format:
    .asciz "%f"

.balign 4
string_3:
    .asciz "Please enter two numbers, and I will tell you which one is bigger!\n"

.balign 4
string_4:
    .asciz "number1: "

.balign 4
string_5:
    .asciz "number2: "

.balign 4
string_6:
    .asciz " is the bigger number!\n"
