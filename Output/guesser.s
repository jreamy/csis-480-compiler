
.text

    B main // branch to main loop


tester: // set up tester

    STR FP, [SP, #12]
    ADD FP, SP, #16
    SUB SP, #4 // make room for 1 variables
    STR LR, [FP, #-8]
    push {R7, R8, R9, R10}
    LDR R10, [FP, #-16] // load R10 : tester.prompt
    MOV R0, R10
    LDR R1, =string_format
    push {LR}
    push {R0}
    push {R1} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    SUB R0, FP, #20
    MOV R1, R0
    LDR R2, =integer_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL __isoc99_scanf
    pop {LR}
    LDR R9, [FP, #-20]

start_if_0:
    LDR R8, [FP, #-12] // load R8 : tester.value
    CMP R9, R8
    BNE else_0
    LDR R0, =1
    MOV R7, R0 // tester.tester : R7
    STR R7, [FP, #-0] // store tester.tester : R7 // resume {'tester.prompt': 'R10', 'tester.guess': 'R9', 'tester.value': 'R8'}
    B endif_0

else_0:
    LDR R0, =0
    LDR R7, [FP, #-0] // load R7 : tester.tester
    MOV R7, R0 // tester.tester : R7
    STR R7, [FP, #-0] // store tester.tester : R7

endif_0:
    pop {R7, R8, R9, R10}
    MOV SP, FP
    LDR LR, [FP, #-8] // load LR : tester..lr
    LDR R0, [FP, #-4] // load R0 : tester..fp
    MOV FP, R0 // resume {} // resume {}
    BX LR // return from function


.global main
.balign 4
main:
    push {FP, LR} // save FP and LR
    MOV FP, SP
    SUB SP, #4 // make room for 1 variables
    LDR R0, =0
    MOV R10, R0 // main.result : R10

start_while_0:
    LDR R1, =1
    CMP R10, R1
    BEQ end_while_0
    push {LR}
    SUB SP, #12
    LDR R0, =5
    push {R0}
    LDR R1, =string_3
    push {R1} // saved all temp registers
    BL tester
    pop {R0}
    pop {LR}
    MOV R10, R0 // main.result : R10

start_if_1:
    LDR R1, =1
    CMP R10, R1
    BNE else_1
    LDR R2, =string_4
    MOV R3, R2
    LDR R0, =string_format
    push {LR}
    push {R3}
    push {R0} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR} // resume {'main.result': 'R10'}
    B endif_1

else_1:
    LDR R2, =string_5
    MOV R3, R2
    LDR R0, =string_format
    push {LR}
    push {R3}
    push {R0} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}

endif_1:
    B start_while_0

end_while_0: // resume {'main.result': 'R10'} // end while 

    LDR R0, =0 // return from main
    MOV SP, FP
    pop {FP, LR}
    BX LR
.data

.balign 4
string_format:
    .asciz "%s"

.balign 4
integer_format:
    .asciz "%d"

.balign 4
float_format:
    .asciz "%f"

.balign 4
string_3:
    .asciz "hi, please guess a number: "

.balign 4
string_4:
    .asciz "Yay, you got it!\n"

.balign 4
string_5:
    .asciz "try again!\n"
