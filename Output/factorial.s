
.text

    B main // branch to main loop


factorial: // set up factorial

    STR FP, [SP, #8]
    ADD FP, SP, #12
    STR LR, [FP, #-8]
    push {R9, R10}
    LDR R0, =1
    MOV R10, R0 // factorial.factorial : R10

start_while_0:
    LDR R9, [FP, #-12] // load R9 : factorial.num
    LDR R1, =0
    CMP R1, R9
    BGE end_while_0
    MUL R2, R9, R10
    MOV R10, R2 // factorial.factorial : R10
    LDR R3, =1
    SUB R0, R9, R3
    MOV R9, R0 // factorial.num : R9
    STR R9, [FP, #-12] // store factorial.num : R9
    B start_while_0

end_while_0: // resume {'factorial.factorial': 'R10', 'factorial.num': 'R9'} // end while 

    STR R10, [FP, #-0] // store factorial.factorial : R10
    pop {R9, R10}
    MOV SP, FP
    LDR LR, [FP, #-8] // load LR : factorial..lr
    LDR R0, [FP, #-4] // load R0 : factorial..fp
    MOV FP, R0 // resume {} // resume {}
    BX LR // return from function


.global main
.balign 4
main:
    push {FP, LR} // save FP and LR
    MOV FP, SP
    SUB SP, #4 // make room for 1 variables
    LDR R0, =string_3
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    SUB R0, FP, #0
    MOV R1, R0
    LDR R2, =integer_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL __isoc99_scanf
    pop {LR}
    LDR R10, [FP, #-0]
    push {LR}
    SUB SP, #12
    MOV R0, R10
    push {R0} // saved all temp registers
    BL factorial
    pop {R0}
    pop {LR}
    MOV R1, R0
    LDR R2, =integer_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    LDR R0, =string_4
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    LDR R0, =0 // return from main
    MOV SP, FP
    pop {FP, LR}
    BX LR
.data

.balign 4
string_format:
    .asciz "%s"

.balign 4
integer_format:
    .asciz "%d"

.balign 4
float_format:
    .asciz "%f"

.balign 4
string_3:
    .asciz "Enter an integer whose factorial shall be calculated: "

.balign 4
string_4:
    .asciz "\n"
