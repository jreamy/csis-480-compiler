
.text

    B main // branch to main loop


fibonacci: // set up fibonacci

    STR FP, [SP, #8]
    ADD FP, SP, #12
    STR LR, [FP, #-8]
    push {R9, R10}

start_if_0:
    LDR R10, [FP, #-12] // load R10 : fibonacci.num
    LDR R0, =1
    CMP R0, R10
    BLT else_0
    LDR R1, =1
    MOV R9, R1 // fibonacci.fibonacci : R9
    STR R9, [FP, #-0] // store fibonacci.fibonacci : R9 // resume {'fibonacci.num': 'R10'}
    B endif_0

else_0:
    push {LR}
    SUB SP, #12
    LDR R0, =1
    SUB R1, R10, R0
    push {R1} // saved all temp registers
    BL fibonacci
    pop {R0}
    pop {LR}
    push {R0}
    push {LR}
    SUB SP, #12
    LDR R0, =2
    SUB R1, R10, R0
    push {R1} // saved all temp registers
    BL fibonacci
    pop {R0}
    pop {LR}
    pop {R1}
    ADD R2, R0, R1
    LDR R9, [FP, #-0] // load R9 : fibonacci.fibonacci
    MOV R9, R2 // fibonacci.fibonacci : R9
    STR R9, [FP, #-0] // store fibonacci.fibonacci : R9

endif_0:
    pop {R9, R10}
    MOV SP, FP
    LDR LR, [FP, #-8] // load LR : fibonacci..lr
    LDR R0, [FP, #-4] // load R0 : fibonacci..fp
    MOV FP, R0 // resume {} // resume {}
    BX LR // return from function


.global main
.balign 4
main:
    push {FP, LR} // save FP and LR
    MOV FP, SP
    SUB SP, #12 // make room for 3 variables
    LDR R0, =string_3
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    SUB R0, FP, #0
    MOV R1, R0
    LDR R2, =integer_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL __isoc99_scanf
    pop {LR}
    LDR R10, [FP, #-0]
    LDR R0, =string_4
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    push {LR}
    SUB SP, #12
    MOV R0, R10
    push {R0} // saved all temp registers
    BL fibonacci
    pop {R0}
    pop {LR}
    MOV R1, R0
    LDR R2, =integer_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    LDR R0, =string_5
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    LDR R0, =string_6
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    push {LR}
    SUB SP, #12
    MOV R0, R10
    push {R0} // saved all temp registers
    BL fibonacci
    pop {R0}
    pop {LR}
    LDR R1, =1
    ADD R2, R1, R0
    MOV R3, R2
    LDR R1, =integer_format
    push {LR}
    push {R3}
    push {R1} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    LDR R0, =string_7
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    push {LR}
    SUB SP, #12
    push {LR}
    SUB SP, #12
    MOV R0, R10
    push {R0} // saved all temp registers
    BL fibonacci
    pop {R0}
    pop {LR}
    LDR R1, =1
    ADD R2, R1, R0
    push {R2} // saved all temp registers
    BL fibonacci
    pop {R0}
    pop {LR}
    MOV R1, R0
    LDR R2, =integer_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    LDR R0, =string_5
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    LDR R0, =0 // return from main
    MOV SP, FP
    pop {FP, LR}
    BX LR
.data

.balign 4
string_format:
    .asciz "%s"

.balign 4
integer_format:
    .asciz "%d"

.balign 4
float_format:
    .asciz "%f"

.balign 4
string_3:
    .asciz "enter int: "

.balign 4
string_4:
    .asciz "fibonacci is: "

.balign 4
string_5:
    .asciz "\n"

.balign 4
string_6:
    .asciz "fibonacci of "

.balign 4
string_7:
    .asciz " is "
