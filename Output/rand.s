
.text

    B main // branch to main loop


print_value: // set up print_value

    STR FP, [SP, #8]
    ADD FP, SP, #12
    STR LR, [FP, #-8]
    push {R9, R10}
    LDR R10, [FP, #-12] // load R10 : print_value.num
    MOV R0, R10
    LDR R1, =integer_format
    push {LR}
    push {R0}
    push {R1} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    LDR R0, =string_3
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    LDR R0, =0
    MOV R9, R0 // print_value.print_value : R9
    STR R9, [FP, #-0] // store print_value.print_value : R9
    pop {R9, R10}
    MOV SP, FP
    LDR LR, [FP, #-8] // load LR : print_value..lr
    LDR R0, [FP, #-4] // load R0 : print_value..fp
    MOV FP, R0 // resume {} // resume {}
    BX LR // return from function

gen_pseudo_rand: // set up gen_pseudo_rand

    STR FP, [SP, #8]
    ADD FP, SP, #12
    SUB SP, #8 // make room for 2 variables
    STR LR, [FP, #-8]
    push {R7, R8, R9, R10}
    LDR R10, [FP, #-12] // load R10 : gen_pseudo_rand.seed
    LDR R0, =127773
    MOV R1, R0
    MOV R2, R10
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL __aeabi_idiv
    pop {LR}
    MOV R9, R0 // gen_pseudo_rand.k1 : R9
    LDR R0, =127773
    MUL R1, R0, R9
    SUB R2, R10, R1
    LDR R3, =16807
    MUL R0, R2, R3
    LDR R1, =2836
    MUL R2, R1, R9
    SUB R3, R0, R2
    MOV R8, R3 // gen_pseudo_rand.ix : R8

start_if_0:
    LDR R1, =0
    CMP R8, R1
    BGE else_0
    LDR R2, =2147483647
    ADD R0, R2, R8
    MOV R8, R0 // gen_pseudo_rand.ix : R8 // resume {'gen_pseudo_rand.seed': 'R10', 'gen_pseudo_rand.k1': 'R9', 'gen_pseudo_rand.ix': 'R8'}

else_0: // end if 

    MOV R7, R8 // gen_pseudo_rand.gen_pseudo_rand : R7
    STR R7, [FP, #-0] // store gen_pseudo_rand.gen_pseudo_rand : R7
    pop {R7, R8, R9, R10}
    MOV SP, FP
    LDR LR, [FP, #-8] // load LR : gen_pseudo_rand..lr
    LDR R0, [FP, #-4] // load R0 : gen_pseudo_rand..fp
    MOV FP, R0 // resume {} // resume {}
    BX LR // return from function


.global main
.balign 4
main:
    push {FP, LR} // save FP and LR
    MOV FP, SP
    SUB SP, #16 // make room for 4 variables
    LDR R0, =string_4
    MOV R1, R0
    LDR R2, =string_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    SUB R0, FP, #0
    MOV R1, R0
    LDR R2, =integer_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL __isoc99_scanf
    pop {LR}
    LDR R10, [FP, #-0]
    MOV R9, R10 // main.continue : R9

start_while_0:
    LDR R0, =0
    CMP R9, R0
    BEQ end_while_0
    push {LR}
    SUB SP, #12
    MOV R0, R10
    push {R0} // saved all temp registers
    BL gen_pseudo_rand
    pop {R0}
    pop {LR}
    LDR R8, [FP, #-4] // load R8 : main.rand
    MOV R8, R0 // main.rand : R8
    push {LR}
    SUB SP, #12
    MOV R0, R8
    push {R0} // saved all temp registers
    BL print_value
    pop {R0}
    pop {LR}
    LDR R7, [FP, #-12] // load R7 : main.void
    MOV R7, R0 // main.void : R7
    MOV R10, R8 // main.seed : R10
    LDR R1, =string_5
    MOV R2, R1
    LDR R3, =string_format
    push {LR}
    push {R2}
    push {R3} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL printf
    pop {LR}
    SUB R0, FP, #8
    MOV R1, R0
    LDR R2, =integer_format
    push {LR}
    push {R1}
    push {R2} // saved all temp registers
    pop {R0}
    pop {R1} // reloaded temp registers in correct order
    BL __isoc99_scanf
    pop {LR}
    LDR R9, [FP, #-8]
    STR R8, [FP, #-4] // store main.rand : R8
    STR R7, [FP, #-12] // store main.void : R7
    B start_while_0

end_while_0: // resume {'main.seed': 'R10', 'main.continue': 'R9'} // end while 

    LDR R0, =0 // return from main
    MOV SP, FP
    pop {FP, LR}
    BX LR
.data

.balign 4
string_format:
    .asciz "%s"

.balign 4
integer_format:
    .asciz "%d"

.balign 4
float_format:
    .asciz "%f"

.balign 4
string_3:
    .asciz "\n"

.balign 4
string_4:
    .asciz "Enter seed number (0 to quit) : "

.balign 4
string_5:
    .asciz "Enter any number to get the next random number or 0 to quit: "
