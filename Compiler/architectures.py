# jreamy17@georgefox.edu
# CSIS 480: Compiler Architectures
# Fall 2019

class Armv7():
    """
    This is the ArmV7 file writer implementation.
    """

    def __init__(self, output_file):
        """
        Initializes a new ArmV7 file writer.

        Arguments:
        output_file -- the file to write code to.
        """

        self.file = output_file

    def write_list(self, *args):
        """
        Writes the list of arguments to the file.

        Arguments:
        *args -- list of arguements to write (comma separated).
        """

        # Write first arg
        self.file.write(args[0])

        # Write other args comma separated
        for arg in args[1:]:
            if not arg is None:
                self.file.write(", " + arg)

    def comment(self, str, *args):
        """
        Writes a comment to the output file allowing debug.  Allows formatting
        the input string with the trailing arguments.

        Arguments:
        str -- the comment to output.
        *args -- the arguments to format the comment with.
        """

        # Format the string if there are input arguments
        if len(args) > 0:
            str = str.format(*args)

        # Write the comment to the file
        self.file.write(" // {}".format(str))

    def data_section(self):
        """
        Declares a data section in the output code.
        """

        self.file.write("\n.data\n")

    def code_section(self):
        """
        Declares a code section in the output code.
        """

        self.file.write("\n.text\n")

    def label(self, label, *args, is_global=False):
        """
        Declares a formatted label in the output file.  Can be local or global.

        Arguments:
        label -- the label to output.
        *args -- any arguments to format the label with.

        Keyword Arguments:
        is_global -- boolean whether the input label is a global.
            (default=False)
        """

        # Clear space
        self.file.write("\n\n")

        # Format the string if any args exist
        if len(args) > 0:
            label = label.format(*args)

        # Declare global if specified
        if is_global:
            self.file.write("\n.global ")
            self.file.write(label)
            self.file.write("\n.balign 4\n")

        # Declare the label
        self.file.write(label)
        self.file.write(":")

    def mk_string(self, label, string):
        """
        Declares the string (in the data section of the program).

        Arguments:
        label -- the label of the string.
        string -- the content of the string
        """

        self.file.write("\n.balign 4\n")
        self.file.write(label)
        self.file.write(":\n    ")
        self.file.write(".asciz ")
        self.file.write(string)
        self.file.write("\n")

    def mk_global(self, label, bytes):
        """
        Declares the global data location (in the data section of the program).

        Arguments:
        label -- the label of the global variable
        bytes -- the bytes (word etc.) of the global variable
        """

        self.file.write("\n.balign 4\n")
        self.file.write(label)
        self.file.write(":\n    ")
        self.file.write(str(bytes))
        self.file.write("\n")

    def branch(self, label, condition=""):
        """
        Writes a branch to the output file.

        Arguments:
        label -- the label to branch to.

        Keyword Arguments:
        condition -- the conditional flag of the branch. (default="")
        """

        self.file.write("\n    ")
        self.file.write("B{} {}".format(condition, label))

    def push(self, *regs, float=False):
        """
        Writes a push command to the output file pushing the input list of
        registers.

        Arguments:
        *regs -- the registers to push to the stack.

        Keyword Arguments:
        float -- boolean whether the input label is a float. (default=False)
        """

        # Write the push instruction
        if len(regs) > 0:
            self.file.write("\n    ")
            if float:
                self.file.write("v")
            self.file.write("push {")
            self.write_list(*regs)
            self.file.write("}")

    def pop(self, *regs, float=False):
        """
        Writes a pop command to the output file popping the input list of
        registers.

        Arguments:
        *regs -- the registers to pop from the stack.

        Keyword Arguments:
        float -- boolean whether the input label is a float. (default=False)
        """

        # Write the pop instruction
        if len(regs) > 0:
            self.file.write("\n    ")
            if float:
                self.file.write("v")
            self.file.write("pop {")
            self.write_list(*regs)
            self.file.write("}")

    def three_op(self, op, dest, src1, src2=None):
        """
        Writes a three op function call to the output file.

        Arguments:
        op -- the operation to output.
        dest -- the destination register.
        src1 -- the first source register.

        Keyword Arguments:
        src2 -- the optional second source register.  (default=None)
        """

        # Write the given op and registers
        self.file.write("\n    ")
        self.file.write(op + " ")
        self.write_list(dest, src1, src2)

    def cmp(self, src1, src2, float=False):
        """
        Writes a compare instruction to the output file.

        Arguments:
        src1 -- the first source register.
        src2 -- the second source register.

        Keyword Arguments:
        float -- boolean whether the input label is a float. (default=False)
        """

        # Write the CMP instruction
        op = "VCMP.f32" if float else "CMP"
        self.three_op(op, src1, src2)
        if float:
            self.file.write("\n    fmstat")

    def mov(self, dst, src, float=False):
        """
        Writes a mov instruction to the output file.

        Arguments:
        dst -- the destination register.
        src -- the source (number, register, or memory location).

        Keyword Arguments:
        float -- boolean whether the input label is a float. (default=False)
        """

        # Load using MOVW if the input is a number
        if src[0] == "#":
            self.ldr(dst, "=" + src[1:], float=float)

        # Load using LDR if the input is a memory location
        elif src[0] == "=":
            self.ldr(dst, src, float=float)

        # Load using MOV is the input is a register
        else:
            op = "VMOV.f32" if float else "MOV"
            self.three_op(op, dst, src)

    def ldr(self, dst, src, *args, float=False):
        """
        Writes an ldr instruction to the output file.

        Arguments:
        dst -- the destination register.
        src -- the source memory location.
        *args -- optional arguments to format the source with.

        Keyword Arguments:
        float -- boolean whether the input label is a float. (default=False)
        """

        # Format the input source if arguments supplied
        if len(args) > 0:
            src = src.format(*args)

        # Write the LDR instruction
        op = "VLDR.f32" if float else "LDR"
        self.three_op(op, dst, src)

    def str(self, src, dst, *args, float=False):
        """
        Writes an str instruction to the output file.

        Arguments:
        src -- the source register.
        dst -- the destination memory location.
        *args -- optional arguments to format the destination with.

        Keyword Arguments:
        float -- boolean whether the input label is a float. (default=False)
        """

        # Format the input destination if arguments supplied
        if len(args) > 0:
            dst = dst.format(*args)

        # Write the STR instruction
        op = "VSTR.f32" if float else "STR"
        self.three_op(op, src, dst)

    def add(self, dest, src1, src2=None, float=False):
        """
        Writes an add instruction to the output file.

        Arguments:
        dest -- the destination register.
        src1 -- the first source resiter or number.

        Keyword Arguments:
        src2 -- the second the source register or number.
        float -- boolean whether the input label is a float. (default=False)
        """

        # Write the ADD instruction
        op = "VADD.f32" if float else "ADD"
        self.three_op(op, dest, src1, src2)

    def sub(self, dest, src1, src2=None, float=False):
        """
        Writes a sub instruction to the output file.

        Arguments:
        dest -- the destination register.
        src1 -- the first source resiter or number.

        Keyword Arguments:
        src2 -- the second the source register or number.
        float -- boolean whether the input label is a float. (default=False)
        """

        # Write the SUB instruction
        op = "VSUB.f32" if float else "SUB"
        self.three_op(op, dest, src1, src2)

    def neg(self, dst, src, float=False):
        """
        Writes a negation instruction to the output file.

        Arguments:
        dst -- the destination register.
        src -- the first source resiter or number.

        Keyword Arguments:
        float -- boolean whether the input label is a float. (default=False)
        """

        # Write the NEG instruction
        op = "VNEG.f32" if float else "NEG"
        self.three_op(op, dst, src)

    def mul(self, dest, src1, src2=None, float=False):
        """
        Writes a mul instruction to the output file.

        Arguments:
        dest -- the destination register.
        src1 -- the first source resiter or number.

        Keyword Arguments:
        src2 -- the second the source register or number.
        float -- boolean whether the input label is a float. (default=False)
        """

        # Write the MUL instruction
        op = "VMUL.f32" if float else "MUL"
        self.three_op(op, dest, src1, src2)

    def div(self, dest, src1, src2=None, float=False):
        """
        Writes a div instruction to the output file.

        Arguments:
        dest -- the destination register.
        src1 -- the first source resiter or number.

        Keyword Arguments:
        src2 -- the second the source register or number.
        float -- boolean whether the input label is a float. (default=False)
        """

        # Write the MUL instruction
        op = "VDIV.f32" if float else "DIV"
        self.three_op(op, dest, src1, src2)
