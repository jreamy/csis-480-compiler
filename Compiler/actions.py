# jreamy17@georgefox.edu
# CSIS 480: Compiler Action Symbols
# Fall 2019

from tokens import *
from symbols import Symbol
from collections import deque
from os import remove

class Action(Symbol):

    """
    The Action extension of the Symbol class is neither a terminal nor a non-
    terminal.  Actions simply act on the compiler given the current state of
    the compiler, generally outputting code.
    """

    _action = True
    _terminal = False
    _non_terminal = False

class ProgramHeader(Action):
    """
    The Program Header Action Symbol.  Generates a Program Header.
    """

    def __call__(self, compiler):
        """
        Generates a Program Header.

        Arguments:
        compiler -- the compiler in which to generate the program header code.
        """

        # Label the code section
        compiler.out.code_section()
        compiler.out.branch("main")
        compiler.out.comment("branch to main loop\n")

class ProgramFooter(Action):
    """
    The Program Footer Action Symbol.  Generates a Program Footer.
    """

    def __call__(self, compiler):
        """
        Generates a Program Footer.

        Arguments:
        compiler -- the compiler in which to generate the program footer code.
        """

        # Label the data section
        compiler.out.data_section()

        # Write the strings used in the program to data memory
        for string, name in compiler.vm.strings.items():
            compiler.out.mk_string(name, string)

class DeclareFunction(Action):
    """
    The Declare Function Action Symbol.  Generates a Function Header.
    """

    def __call__(self, compiler):
        """
        Generates a Function Header.

        Arguments:
        compiler -- the compiler in which to generate the function header code.
        """

        # Label the function
        compiler.out.label(compiler._prev_id)
        compiler.vm.set_scope(compiler._prev_id)

        # Add the function to the vm
        compiler.vm.add_function(compiler._prev_id, compiler.prev_type)
        compiler.vm.add_var(compiler._prev_id, compiler.prev_type)

        # Save the register state
        compiler.vm.regs.push_reg_state()
        compiler.vm.fregs.push_reg_state()

        # Allocate space for the link register and frame pointer
        compiler.vm.add_var(".fp", Integer)
        compiler.vm.add_var(".lr", Integer)
        compiler.out.comment("set up {}\n", compiler._prev_id)
        compiler.vm.undeclared_vars = 0

        compiler.vm.params.clear()

class WriteFunction(Action):
    """
    The Write Function Action Symbol. Begins the process of writing the function
    to a separate file.
    """

    def __call__(self, compiler):
        """
        Begins the process of writing the function to a temporary file.

        Arguments:
        compiler -- the compiler in which to begin writing a function.
        """

        # Store the link register
        lr_name = compiler.vm.get_var_name(".lr")
        mem = compiler.vm.regs.get_mem_loc(lr_name)
        compiler.out.str("LR", "[FP, #-{}]", mem)

        # Set the output file to the temporary function file
        compiler.out.file = open(compiler.function_file, "w+")

class Function(Action):
    """
    The Function Action Symbol.  Adds a new function call to the VM.  Is not a
    singleton.
    """

    _singleton = False

    def __init__(self):
        """
        Creates a new Function object.
        """

        # Create a blank function
        self.args = []
        self.postfix = deque()
        self.operands = deque()
        self.operators = deque()

    def __call__(self, compiler):
        """
        Begins a Function call in the VM.

        Arguments:
        compiler -- the compiler in which to call the function.
        """

        # Get the name and parameters of the function
        compiler.vm.postfix.pop()
        self.name = compiler._prev_id
        self.params = compiler.vm.get_params(self.name)
        self.num_params = len(self.params)

        # Put the function on the call stack
        compiler.vm.call_stack.append(self)
        compiler.vm.regs.push_all_tmps()
        compiler.vm.regs.clear_all_tmps()
        compiler.vm.regs.freeze_tmp_stack()

        # Save the LR and move the stack pointer allocating space
        compiler.out.push("LR")
        compiler.out.sub("SP", "#{}".format(12))

class EvalFunction(Action):
    """
    The Evalate Function Action Symbol.  Generates code enacting the function
    call once all parameters have been computed.
    """

    def __call__(self, compiler):
        """
        Calls a Function in the VM.

        Arguments:
        compiler -- the compiler in which to call the function.

        Raises:
        SyntaxError -- if too many arguments have been passed.
        """

        # Get the function that was being called
        function = compiler.vm.call_stack.pop()

        # Raise an error if the wrong number of parameters are passed
        if (len(function.args) != function.num_params):
            err = compiler.lexer.display_error()
            line = compiler.lexer._last_line_number
            raise SyntaxError(("Incorrect function arguments on line {}"
                " around: \n{}".format(line, err)))

        # Call the function!
        compiler.vm.call(function.name, reg_pass=False)

        # Get the function return
        (ret, type) = compiler.vm.pop_operand()
        compiler.vm.end_param()
        return_type = compiler.vm.get_function_type(function.name)

        # Push the return operand to the vm stack
        if return_type == Integer or return_type == String:
            ret = compiler.vm.regs.get_tmp_reg(reg=ret)
            compiler.vm.push_operand(("RX", return_type))
        elif return_type == Float:
            ret = compiler.vm.fregs.get_tmp_reg(reg=ret)
            compiler.vm.push_operand(("SX", Float))

class FunctionFooter(Action):
    """
    The Function Action Symbol.  Generates a Function Footer.
    """

    def __call__(self, compiler):
        """
        Generates a Function Footer.

        Arguments:
        compiler -- the compiler in which to generate the function footer code.
        """

        # Get the function name (which is the current scope)
        var = compiler.vm.get_var_name(compiler.vm.get_scope())

        # Get the retunr type of the function
        datatype = compiler.vm.get_function_type(compiler.vm.get_scope())

        # Get the associated registers
        regs = compiler.vm.fregs if datatype == Float else compiler.vm.regs

        # Default return value to 0
        if not regs.was_inited(var):
            reg = regs.get_var_reg(var)
            compiler.out.mov(reg, "#0", float=(datatype==Float))
            regs.edit_var(var)

        # Store the return value
        regs.store_var(var)

        # Close the temporary file and switch back to the main file
        compiler.out.file.close()
        compiler.out.file = compiler.file

        # Push all the registers used before inserting the function call
        compiler.out.push(*compiler.vm.regs.get_regs_used())
        compiler.out.push(*compiler.vm.fregs.get_regs_used(), float=True)

        # Inseret function text
        with open(compiler.function_file, "r+") as f:
            for line in f.readlines():
                compiler.file.write(line)

        # Delete the temporary file
        remove(compiler.function_file)

        # Pop all regs used at the end
        compiler.out.pop(*compiler.vm.fregs.get_regs_used(), float=True)
        compiler.out.pop(*compiler.vm.regs.get_regs_used())

        # Restore the stack pointer
        compiler.out.mov("SP", "FP")

        # Restore the frame pointer and link register
        lr = compiler.vm.get_var_name(".lr")
        fp = compiler.vm.get_var_name(".fp")
        compiler.vm.regs.load_var(lr, "LR", force=True)
        compiler.vm.regs.load_var(fp, "R0", force=True)
        compiler.out.mov("FP", "R0")

        # Reset the registers used
        compiler.vm.regs.clear_regs_used()
        compiler.vm.fregs.clear_regs_used()
        compiler.vm.regs.resume_reg_state()
        compiler.vm.fregs.resume_reg_state()

        # Return from function
        compiler.out.branch("LR", "X")
        compiler.out.comment("return from function")

class InitParam(Action):
    """
    The Initialize Parameter Action Symbol. Adds a variable to the function
    parameter list.
    """

    def __call__(self, compiler):
        """
        Adds a variable to the function parameter list.

        Arguments:
        compiler -- the compiler in which to add parameters.
        """

        # Add the variable without increasing the space to allocate on the stack
        var = compiler.vm.get_var_name(compiler._prev_id)
        compiler.vm.params.append(compiler.prev_type)
        compiler.vm.undeclared_vars = 0
        compiler.vm.init_var(var, compiler.prev_type)

class DeclareParams(Action):
    """
    The Declare Parameters Action Symbol. Allocates space on the stack for all
    the function parameters.
    """

    def __call__(self, compiler):
        """
        Allocates space on the stack for all the function parameters.

        Arguments:
        compiler -- the compiler in which to declare parameters.
        """

        # Associate the current parameters with the correct function
        compiler.vm.set_params(compiler.vm.get_scope(),
            list(compiler.vm.params))

        # Allocate space on the stack
        compiler.out.str("FP", "[SP, #{}]", 4+4*len(compiler.vm.params))
        compiler.out.add("FP", "SP", "#" + str(8+4*len(compiler.vm.params)))

        # Set the number of variables to count the parameters and the 3 arg
        #  padding
        compiler.vm.num_vars = len(compiler.vm.params) + 3
        compiler.vm.params.clear()

class BeginParam(Action):
    """
    The Begin Parameter Action Symbol. Sets the compiler VM to treat all new
    operators and operands as part of the parameter.
    """

    def __call__(self, compiler):
        """
        Sets the compiler VM to treat all new operators and operands as part
        of the parameter.

        Arguments:
        compiler -- the compiler in which to begin a parameter evaluation.
        """

        compiler.vm.begin_param()

class EvalParam(Action):
    """
    The Evaluate Parameter Action Symbol. Evaluates the current state of the
    parameter arguments in the VM.
    """

    def __call__(self, compiler):
        """
        Evaluates the current state of the parameter arguments in the VM.

        Arguments:
        compiler -- the compiler in which to evaluate a parameter.

        Raises:
        SyntaxError -- if too many arguments have been passed.
        """

        # Get the current function
        function = compiler.vm.call_stack[-1]

        # Evaluate the parameter and end the param in the vm
        compiler.vm.eval_postfix()
        op = compiler.vm.pop_operand(load_tmp=True)
        compiler.vm.end_param()

        # Raise an error if too many parameters have been passed
        if (len(function.args) >= function.num_params):
            err = compiler.lexer.display_error()
            line = compiler.lexer._last_line_number
            raise SyntaxError(("Too many function arguments on line {}"
                " around: \n{}".format(line, err)))

        # Push the operand to the call stack
        compiler.out.push(op[0], float=(op[1]==Float))
        function.args.append(op)

class MainHeader(Action):
    """
    The Main Function Header Action Symbol.  Generates a Main Function Header.
    """

    def __call__(self, compiler):
        """
        Generates a Main Function Header.

        Arguments:
        compiler -- the compiler in which to generate the main header code.
        """

        # Set the scope
        compiler.vm.set_scope("main")
        compiler.out.label("main", is_global=True)

        # Set up frame pointer and preserve link register
        compiler.out.push("FP", "LR")
        compiler.out.comment("save FP and LR")
        compiler.out.mov("FP", "SP")

class MainFooter(Action):
    """
    The Main Function Footer Action Symbol.  Generates a Main Function Footer.
    """

    def __call__(self, compiler):
        """
        Generates a Main Function Footer.

        Arguments:
        compiler -- the compiler in which to generate the main footer code.
        """

        # Set up the return from call to main
        compiler.out.mov("R0", "#0")
        compiler.out.comment("return from main")

        # Restore the stack pointer, the frame pointer and the link register
        compiler.out.mov("SP", "FP")
        compiler.out.pop("FP", "LR")

        # Return from main
        compiler.out.branch("LR", "X")

class FormatPrintExpression(Action):
    """
    The Format Print Expression Action Symbol.  Generates code to print an
    expression.
    """

    def __call__(self, compiler):
        """
        Generates code to print an expression.

        Arguments:
        compiler -- the compiler in which to generate the print code.
        """

        # Ensure the operand is in a tmp reg
        operand = compiler.vm.pop_operand()
        compiler.vm.push_operand(operand, load_tmp=True)

        # Call string print if needed
        if operand[1] == String:
            compiler.vm.push_operand(("=string_format", Integer), load_tmp=True)
            compiler.vm.call("printf", args=2, rets=0)

        # Call integer print if needed
        elif operand[1] == Integer:
            compiler.vm.push_operand(("=integer_format", Integer), load_tmp=True)
            compiler.vm.call("printf", args=2, rets=0)

        # Call float print if needed
        elif operand[1] == Float:
            # Get temporary registers for float --> double conversion & printing
            tmp1 = compiler.vm.regs.get_tmp_reg(reg="R3")
            tmp2 = compiler.vm.regs.get_tmp_reg(reg="R2")
            tmp3 = compiler.vm.regs.get_tmp_reg(reg="R1")

            # Get the actual float to print and the two parts of D0
            (reg, flt) = compiler.vm.pop_operand(load_tmp=True)
            s0 = compiler.vm.fregs.get_tmp_reg(reg="S0")
            s1 = compiler.vm.fregs.get_tmp_reg(reg="S1")

            # Convert the float to a double and store in the correct regs
            compiler.out.three_op("fcvtds", "D0", reg)
            compiler.out.three_op("vmov", tmp2, tmp1, "D0")

            # Make room on the vm stack
            compiler.vm.push_operand((tmp1, Integer))
            compiler.vm.push_operand((tmp1, Integer))
            compiler.vm.push_operand((tmp1, Integer))

            # Call the print of the float
            compiler.vm.push_operand(("=float_format", String), load_tmp=True)
            compiler.vm.call("printf", args=4, rets=0, double_align=True)

            # Free the tmp regs
            compiler.vm.fregs.pop_tmp_reg(reg="S0")
            compiler.vm.fregs.pop_tmp_reg(reg="S1")

class ScanInput(Action):
    """
    The Scan Input Action Symbol.  Generates code to scan an integer, float or
    string input.
    """

    def __call__(self, compiler):
        """
        Generates code to scan an integer, float or string input.

        Arguments:
        compiler -- the compiler in which to generate the scan code.
        """

        # Get the variable to write to
        var = compiler.vm.get_var_name(compiler._prev_id)
        datatype = compiler.vm.get_var_type(var)

        # Scan an integer if needed
        if datatype == Integer:
            # Get the pointer to the memory location of the variable
            mem = compiler.vm.regs.get_mem_loc(var)
            tmp = compiler.vm.regs.get_tmp_reg()
            compiler.out.sub(tmp, "FP", "#{}".format(mem))
            compiler.vm.push_operand((tmp, Integer), load_tmp=True)

            # Call scanf for an integer
            compiler.vm.push_operand(("=integer_format", String), load_tmp=True)
            compiler.vm.regs.pop_tmp_reg(reg=tmp)
            compiler.vm.call("__isoc99_scanf", args=2, rets=0)

            # Reload the new variable value
            reg = compiler.vm.regs.get_var_reg(var)
            compiler.out.ldr(reg, "[FP, #-{}]", mem)


        # Scan a float if needed
        elif datatype == Float:
            # Get the pointer to the memory location of the variable
            mem = compiler.vm.fregs.get_mem_loc(var)
            tmp = compiler.vm.regs.get_tmp_reg()
            compiler.out.sub(tmp, "FP", "#{}".format(mem))
            compiler.vm.push_operand((tmp, Integer), load_tmp=True)

            # Call scanf for a float
            compiler.vm.push_operand(("=float_format", String), load_tmp=True)
            compiler.vm.regs.pop_tmp_reg(reg=tmp)
            compiler.vm.call("__isoc99_scanf", args=2, rets=0)

            # Reload the new variable value
            reg = compiler.vm.fregs.get_var_reg(var)
            compiler.out.ldr(reg, "[FP, #-{}]", mem, float=True)

        # Scan a string if needed
        elif datatype == String:
            # Get the pointer to the memory location of the variable
            mem = compiler.vm.regs.get_mem_loc(var)
            tmp = compiler.vm.regs.get_tmp_reg()
            compiler.out.sub(tmp, "FP", "#{}".format(mem))
            compiler.vm.regs.pop_tmp_reg(reg=tmp)
            compiler.vm.push_operand((tmp, String), load_tmp=True)

            # Call the custom read_string function
            #  (note that it needs to be inserted)
            compiler.vm.call("__read_string", args=1, rets=0)
            if not "__read_string.s" in compiler.functions_to_insert:
                compiler.functions_to_insert.append("__read_string.s")

            # Store the string pointer returned
            reg = compiler.vm.regs.get_var_reg(var)
            compiler.out.mov(reg, "R0")
            compiler.vm.regs.edit_var(var)
            compiler.out.comment("stored string")

class DeclareVariable(Action):
    """
    The Declare Variable Action Symbol.  Adds a variable to the internal state
    of the compiler.
    """

    def __call__(self, compiler):
        """
        Adds a variable to the internal state of the compiler.

        Arguments:
        compiler -- the compiler in which to declare the variable.
        """

        compiler.vm.add_var(compiler._prev_id, compiler.prev_type)

class DeclareVariableList(Action):
    """
    The Declare Variable List Action Symbol. Generates code making room on the
    stack for local variables.
    """

    def __call__(self, compiler):
        """
        Generates code making room on the stack for local variables.

        Arguments:
        compiler -- the compiler in which to generate code.
        """

        # Generate code moving the stack pointer
        compiler.vm.declare_vars()

class AssignmentId(Action):
    """
    The Assignment Id Action Symbol.  Stores the current identifier in the
    compiler as the assignment id for the current expression.
    """

    def __call__(self, compiler):
        """
        Stores the current identifier in the compiler as the assignment id for
        the current expression.

        Arguments:
        compiler -- the compiler in which to store the assignment id.
        """

        var = compiler.vm.get_var_name(compiler._prev_id)
        compiler._assign_id = var

class AssignVariable(Action):
    """
    The Assign Variable Action Symbol.  Generates code assigning the current
    evaluated temp expression to the appropriate variable.
    """

    def __call__(self, compiler):
        """
        Generates code assigning the current evaluated temp expression to the
        appropriate variable.

        Arguments:
        compiler -- the compiler in which to generate code.
        """

        # Get the current variable
        var = compiler._assign_id

        # Get the source and destination
        operand = compiler.vm.pop_operand()

        # String assignment
        if operand[1] == String:
            # Get the destination register
            dst = compiler.vm.regs.get_var_reg(var)

            # Generate code assigning the value to the variable
            compiler.out.mov(dst, operand[0])
            compiler.out.comment("{} : {}", var, dst)

            # Initialize the variable
            compiler.vm.regs.init_var(var)
            compiler.vm.regs.edit_var(var)

        # Integer assignment
        elif operand[1] == Integer:
            # Get the destination register
            dst = compiler.vm.regs.get_var_reg(var)

            # Generate code assigning the value to the variable
            compiler.out.mov(dst, operand[0])
            compiler.out.comment("{} : {}", var, dst)

            # Initialize the variable
            compiler.vm.regs.init_var(var)
            compiler.vm.regs.edit_var(var)

        # Float assignment
        elif operand[1] == Float:
            # Get the destination register
            dst = compiler.vm.fregs.get_var_reg(var)

            # Generate code assigning the value to the variable
            compiler.out.mov(dst, operand[0], float=True)
            compiler.out.comment("{} : {}", var, dst)

            # Initialize the variable
            compiler.vm.fregs.init_var(var)
            compiler.vm.fregs.edit_var(var)

class PushPostfix(Action):
    """
    The Push Postfix Action Symbol.  Pushes the current token to the internal
    postfix stack for later computation.
    """

    def __call__(self, compiler):
        """
        Pushes the current token to the internal postfix stack for later
        computation.

        Arguments:
        compiler -- the compiler in which to push a variable.
        """

        compiler.vm.push_postfix(compiler.current_token)

class PushOperator(Action):
    """
    The Push Operator Action Symbol.  Pushes the operator token to the internal
    operator stack for later computation.
    """

    def __call__(self, compiler):
        """
        Pushes the current token to the internal operator stack for later
        computation.

        Arguments:
        compiler -- the compiler in which to push an operator.
        """

        compiler.vm.push_operator(compiler.current_token)

class EvalSign(Action):
    """
    The Push Sign Operator Symbol. Pushes a sign into the internal stack, to be
    applied to the next postfix operand pushed.
    """

    def __call__(self, compiler):
        """
        Pushes a sign into the internal stack, to be applied to the next
        postfix operand pushed.

        Arguments:
        compiler -- the compiler in which to push an operator.
        """

        # Check the input token is a negation
        if isinstance(compiler.current_token, SubOp):
            prev_op = compiler.vm.operators.pop()

            # If the previous was not a negate, then negate, otherwise double
            #  negatives don't get pushed back onto the stack
            if not isinstance(prev_op, NegOp):
                compiler.vm.push_operator(prev_op)
                compiler.vm.push_operator(NegOp())

class EvalTerm(Action):
    """
    The Eval Term Action Symbol.  Generates code computing the current
    expression.
    """

    def __call__(self, compiler):
        """
        Generates code computing the current expression.

        Arguments:
        compiler -- the compiler in which to generate code.
        """

        compiler.vm.eval_postfix()

class StartElse(Action):
    """
    The Start Else Action Symbol.  Generates code setting up an else clause.
    """

    def __call__(self, compiler):
        """
        Generates code setting up an else clause.

        Arguments:
        compiler -- the compiler in which to generate code.
        """

        # Restore the state of the registers before the if block
        compiler.vm.regs.pop_reg_state()

        # Restore the variable scope
        compiler.vm.pop_scope()

        # Resume the internal register state after the comparison
        compiler.vm.regs.resume_reg_state()

        # Generate the branch ending the if clause
        compiler.out.branch(compiler.vm.pop_label())

        # Label the beginning of the else clause
        compiler.out.label(compiler.vm.pop_label())

class EndElse(Action):
    """
    The End Else Action Symbol.  Generates code ending an else clause.
    """

    def __call__(self, compiler):
        """
        Generates code ending an else clause.

        Arguments:
        compiler -- the compiler in which to generate code.
        """

        # Restore the state of the registers before the if block
        compiler.vm.regs.pop_reg_state()

        # Label the end of the else clause
        compiler.out.label(compiler.vm.pop_label())

        # Restore the variable scope
        compiler.vm.pop_scope()


class SkipElse(Action):
    """
    The Skip Else Action Symbol.  Generates code skipping an else clause.
    """

    def __call__(self, compiler):
        """
        Generates code skipping an else clause.

        Arguments:
        compiler -- the compiler in which to generate code.
        """

        # Restore the state of the registers before the if block
        compiler.vm.regs.pop_reg_state()

        # Restore the variable scope
        compiler.vm.pop_scope()
        compiler.vm.pop_scope()

        # Resume the internal register state after the comparison, ignore excess
        compiler.vm.regs.resume_reg_state()
        compiler.vm.regs.pop_reg_state(restore=False)

        # Label the end of the if clause, ignore excess
        compiler.vm.pop_label()
        compiler.out.label(compiler.vm.pop_label())
        compiler.vm.pop_label()
        compiler.out.comment("end if \n")

class EndWhile(Action):
    """
    The End While Action Symbol.  Generates code ending a while loop.
    """

    def __call__(self, compiler):
        """
        Generates code ending a while loop.

        Arguments:
        compiler -- the compiler in which to generate code.
        """

        # Restore the state of the registers before the while block
        compiler.vm.regs.pop_reg_state()

        # Branch back to the beginning of the loop
        compiler.out.branch(compiler.vm.pop_label())

        # Label the end of the loop
        compiler.out.label(compiler.vm.pop_label())

        # Resume the internal register state after the comparison
        compiler.vm.regs.resume_reg_state()
        compiler.out.comment("end while \n")

        # Restore the variable scope
        compiler.vm.pop_scope()
