
.balign 4
__read_string:
    // This is the compiled gcc output to a read_string function modified from:
    //  https://www.mkyong.com/c/how-to-handle-unknow-size-user-input-in-c/
    push	{r7, lr}
	sub	sp, sp, #24
	add	r7, sp, #0
	str	r0, [r7, #4]
	movs	r3, #128
	str	r3, [r7, #12]
	movs	r3, #0
	str	r3, [r7, #20]
	ldr	r0, [r7, #12]
	bl	malloc
	mov	r3, r0
	str	r3, [r7, #4]
	ldr	r3, [r7, #12]
	str	r3, [r7, #20]
	ldr	r3, [r7, #4]
	cmp	r3, #0
	beq	__read_string.L2
	mov	r3, #-1
	str	r3, [r7, #8]
	movs	r3, #0
	str	r3, [r7, #16]
	b	__read_string.L3
__read_string.L5:
	ldr	r3, [r7, #16]
	adds	r2, r3, #1
	str	r2, [r7, #16]
	ldr	r2, [r7, #4]
	add	r3, r3, r2
	ldr	r2, [r7, #8]
	uxtb	r2, r2
	strb	r2, [r3]
	ldr	r2, [r7, #16]
	ldr	r3, [r7, #20]
	cmp	r2, r3
	bne	__read_string.L3
	ldr	r2, [r7, #16]
	ldr	r3, [r7, #12]
	add	r3, r3, r2
	str	r3, [r7, #20]
	ldr	r0, [r7, #4]
	ldr	r1, [r7, #20]
	bl	realloc
	str	r0, [r7, #4]
__read_string.L3:
	bl	getchar
	str	r0, [r7, #8]
	ldr	r3, [r7, #8]
	cmp	r3, #10
	beq	__read_string.L4
	ldr	r3, [r7, #8]
	cmp	r3, #-1
	bne	__read_string.L5
__read_string.L4:
	ldr	r2, [r7, #4]
	ldr	r3, [r7, #16]
	add	r3, r3, r2
	movs	r2, #0
	strb	r2, [r3]
__read_string.L2:
	ldr	r3, [r7, #4]
	mov	r0, r3
	adds	r7, r7, #24
	mov	sp, r7
	@ sp needed
	pop	{r7, pc}
