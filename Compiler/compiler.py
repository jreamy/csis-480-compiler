# jreamy17@georgefox.edu
# CSIS 480: Compiler Syntactic Analyzer
# Fall 2019

from collections import deque
from tokens import EndOfFile
from symbols import Script

from lexical_analysis import HansenStout
from register_allocator import RegisterAllocator
from virtual_machine import VirtualMachine
from architectures import Armv7

class Compiler():
    """
    The Syntax Analyzer that analyzes the syntactic output of a top level
    Lexical Analyzer.
    """

    def push_symbol(self, symbol):
        """
        Pushes the symbol onto the symbol stack.
        """

        self.symbol_stack.append(symbol)

    def pop_symbol(self):
        """
        Pops the symbol off the symbol stack and returns.

        Returns:
        The symbol from the top of the symbol stack.
        """

        return self.symbol_stack.pop()

    def not_empty(self):
        """
        Returns whether the symbol stack is empty.

        Returns:
        True if the length of the symbol stack > 1.
        """

        return len(self.symbol_stack) > 1

    def compile(self, inputfile, outputfile="output.s", show_all=False):
        """
        Compiles the given input file to the given output file

        Arguments:
        inputfile -- the filename of the input file to compile.

        Keyword Arguments:
        outputfile -- the filename of the output file.
        show_all -- whether to print line number, Token, and current Symbol for
            debug. (default=False)

        Raises:
        SyntaxError -- if the input file contains syntactic or lexical errors.
        """

        # Initilize the stack
        self.symbol_stack = deque()

        self.push_symbol(EndOfFile)
        self.push_symbol(Script)

        # Initialize the input and output files and internal state
        self.lexer = HansenStout()(inputfile)
        self.file = open(outputfile, "w+")
        self.tmp_file = outputfile[:-2]+".tmp"
        self.function_file = outputfile[:-2]+".f"

        # Set up the internal vm and appropriate architecture
        self.out = Armv7(self.file)
        self.vm = VirtualMachine(self)

        # Get the first token
        self.current_token = self.lexer.next_token()

        # Function insertion
        self.functions_to_insert = []

        # Run until the end of the input
        while not isinstance(self.current_token, EndOfFile) or self.not_empty():

            # Get the symbol at the top of the stack
            current_type = self.pop_symbol()

            # Print the line and token if in debug
            if show_all:
                print(self.lexer._line_number, current_type.__name__,
                    self.current_token)

            # Check token equality if terminal
            if current_type.is_terminal():
                # If equal, get the next token
                if isinstance(self.current_token, current_type):
                    if self.current_token.is_action():
                        self.current_token(self)

                    self.current_token = self.lexer.next_token()

                # Otherwise, raise an error
                else:
                    self.lexer.display_error()
                    self.file.close()
                    self.raise_syntax_error(current_symbol)

            # If non-terminal, load the next symbols onto the stack
            elif current_type.is_non_terminal():
                # Get the instance of the current symbol
                current_symbol = current_type.get_instance()

                # Get the next pattern of symbols given the current token and
                #  symbol
                mapping = current_symbol[self.current_token]

                # Raise an error if there is no mapping for the symbol, token
                #  pair
                if mapping is None:
                    self.file.close()
                    self.raise_syntax_error(current_symbol)

                # Otherwise, add the production to the stack
                for symbol in reversed(mapping):
                    self.push_symbol(symbol)

            # Check the current symbol to see if it's an action
            elif current_type.is_action():
                # If it is, call the action on the compiler
                current_type.get_instance()(self)

        # Write in each external function needed
        for function in self.functions_to_insert:
            with open(function) as f:
                for line in f.readlines():
                    self.file.write(line)

        # Close the output file
        self.file.close()

    def raise_syntax_error(self, state):
        """
        Raises a SyntaxError displaying the given syntax error, and the expected
        tokens.

        Raises:
        SyntaxError -- explains what tokens were expected and where.
        """

        # Display the acutal lexeme if it was preserved, otherwise the name of
        #  the current token
        token = self.current_token

        # Raise the formatted syntax error
        raise SyntaxError(("Invalid Syntax on line {}, expected {}"
            " in state {} but found '{}'\n{}".format(
                self.lexer._last_line_number,
                state.get_first(), state, token,
                self.lexer.display_error())))
