# jreamy17@georgefox.edu
# CSIS 480: Compiler Lexial Analyzers
# Fall 2019

from abc import ABC, abstractmethod
from tokens import *

class MachineState():
    """
    Internal representation of states for the LexicalAnalyzer class.  Each acts
    as a dictionary indexable by an input character that returns the next
    state, or None if there is no mapping for that character.
    """

    def __init__(self, name, token=None, mapping = lambda x: None):
        """
        Initialization of the MachineState object.

        Arguments:
        name -- the name or number of the state.

        Keyword Arguments:
        token -- the type of token to return. (default None)
        mapping -- the map function from one state to another.  Allows a
            MachineState object to be indexed like a dictionary.
        """

        self._name = name
        self._token = token
        self._mapping = mapping

    def __str__(self):
        """
        Returns a string representation of the MachineState.
        """

        return "MachineState(name={}, token={})".format(self._name,
            self._token.__name__)

    def __getitem__(self, index):
        """
        Calls the mapping method specified at initialization.  Used for
        getting the next state for the given character.
        """

        return self._mapping(index)

class LexicalAnalyzer(ABC):
    """
    Abstract LexicalAnalyzer class that inputs a text file and returns an
    iterator of the file that outputs the lexical tokens present.  Must be
    extended for a given language by defining an __init__ function which
    constructs the DFA of the class.
    """

    @abstractmethod
    def __init__(self):
        """
        This function must be implemented by any child classes.  It should
        define the DFA used for lexical analysis.
        """

        pass

    def __iter__(self):
        """
        Initializes the LexicalAnalyzer as a file iterator.
        """

        return self

    def __next__(self):
        """
        Returns the next token in the file.  Throws a StopIteration exception
        when the EndOfFile token is reached.
        """

        # This is HOW a python iterator works, not a preference
        if isinstance(self._current_token, EndOfFile):
            raise StopIteration

        return self.next_token()

    def __call__(self, file):
        """
        Allows a LexicalAnalyzer to be called on the given file.
        """

        return self.tokenize(file)

    def reset(self):
        """
        Resets the LexicalAnalyzer.  It must then be reinitialized by calling
        tokenize on a file.
        """

        self._current_state = None
        self._current_token = None
        self._current_character = None
        self._previous_character = None
        self._line_number = 1
        self._char_number = 1
        self._last_line_number = 1

    def next_char(self):
        """
        Reads the next character from the file if the previous character has
        been consumed.
        """

        # Updates the next character if the current one has been cleared
        if self._current_character is None:
            # Gets the next character from the file
            self._current_character = self._file.read(1)

            # If it's a newline, increase line count & reset character
            if (self._previous_character == "\n"):
                self._char_number = 0
                self._line_number += 1
            else:
                # Else increase character count
                self._char_number += 1

            self._previous_character = self._current_character

    def tokenize(self, file):
        """
        Prepares the LexicalAnalyzer to be used as an iterator over the given
        file.
        """

        # Open the file
        self._file = open(file)
        self._filename = file

        return self

    def has_next(self):
        """
        Returns true if there are more characters in the file.
        """

        return self._current_character != ""

    def next_token(self):
        """
        Returns the next token while set as an iterator.
        """

        # Setup. Start state with blank ouput
        self._current_state = self._start
        self._current_token = None
        lexeme = ""

        # Get the next character from file if needed
        self.next_char()

        # Iterate character at a time through the file parsing next token
        while self.has_next() and self._current_token is None:
            # Get the next character and the next state
            next_state = self._current_state[self._current_character]

            # If in the start state, reset the lexeme
            if self._current_state == self._start:
                lexeme = ""

            # If there is a following state
            if next_state is not None:
                # Append to the lexeme and clear the current character
                lexeme = lexeme + self._current_character
                self._current_character = None
                self._current_state = next_state

            # No following state, but currenly accepting state
            elif self._current_state._token is not None:
                # Return the token
                self._current_token = self._current_state._token(lexeme)

            # No following state, and not accepting
            else:
                # Raise an error
                self.raise_lexical_error()

            # Get the next character from file if needed
            self.next_char()

        # If there are no more characters, but in an accepting state
        if self._current_state._token is not None:
            # Return the current token
            self._current_token = self._current_state._token(lexeme)

        # If there are no more character, but not in accepting state
        elif self._current_state != self._start:
            # Raise an error
            self.raise_eof_error()

        else:
            # Close the file when there are no more characters
            self._file.close()
            self._file = None
            self._current_token = EndOfFile.get_instance()

        # Update last line & character
        self._last_line_number = self._line_number

        return self._current_token

    def display_error(self, backup=0):
        """
        Displays the line from the current file that caused the current error,
        points at the errant character with a '^' in the line below.

        Returns:
        A string displaying the line causing the current error.
        """

        # Reopen the file
        with open(self._filename) as f:
            # Read the whole file
            file_data = f.readlines()

            # Get the appropriate line number (capped)
            line_num = self._last_line_number - 1 - backup
            if line_num >= len(file_data):
                line_num = -1

            # Print the line
            output = (file_data[line_num][: -1]) + "\n"

            # Print a ^ character pointing at the errant character
            output += (" " * (self._char_number - 1) + "^")

        return output

    def raise_lexical_error(self):
        """
        Raises a lexical error displaying the last line that successfully
        parsed, and the state that the parser is currently in.
        """

        # Raise an explanatory error
        raise SyntaxError(("No mapping for '{}' character on line " +
            "{} when in {} state\n{}").format(self._current_character,
                self._line_number-1, self._current_state._name,
                self.display_error()))

    def raise_eof_error(self):
        """
        Raises an End Of File (EOF) error displaying the last line that
        successfully parsed, and the state that the parser has been in.
        """

        # Raise an explanatory error
        raise SyntaxError(("EOF while in {} state since line {}\n{}".format(
            self._current_state._name, self._last_line_number,
            self.display_error())))

class HansenStout(LexicalAnalyzer):
    """
    The HansenStout Lexical Analyzer.
    """

    def __init__(self):
        """
        Generates all the states of the HansenLite Lexical Analyzer and
        initializes the instance.
        """

        # Start state initialization
        self._start = MachineState("start", mapping = lambda x: (
            self._number if x.isdigit() else
            self._identifier if x.isalpha() else
            self._in_string if x == '"' else
            self._add_op if x == "+" else
            self._sub_op if x == "-" else
            self._lt if x == "<" else
            self._eq if x == "=" else
            self._gt if x == ">" else
            self._mul_op if x == "*" else
            self._div_op if x == "/" else
            self._in_assign if x == ":" else
            self._statement_sep if x == ";" else
            self._left_paren if x == "(" else
            self._right_paren if x == ")" else
            self._start if x in [" ", "\t", "\n"] else
            self._in_comment if x == "{" else
            self._list_sep if x  == "," else
            None))

        # Identifier state initialization
        self._identifier = MachineState("identifier",
            self.identifier_disambiguator, mapping = lambda x : (
                self._identifier if x.isalnum() or x == "_" else
                None))

        # Number state initialization
        self._number = MachineState("number", IntegerConst, mapping =
            lambda x : (
                self._number if x.isdigit() else
                self._float if x == "." else
                None))

        # Float exponent e state
        self._float = MachineState("float", FloatConst, mapping =
            lambda x : (
                self._float if x.isdigit() else
                self._float_e if x in ["e", "E"] else
                None))

        # Allows a float to be signed
        self._float_e = MachineState("float extension", mapping =
            lambda x : (
                self._float_exp if x in ["+", "-"] else
                self._float_exp if x.isdigit() else
                None))

        # Float Exponent state
        self._float_exp = MachineState("float exponent", FloatConst, mapping =
            lambda x : (
                self._float_exp if x.isdigit() else
                None))

        # Mid-string state initialization (things after the first ")
        self._in_string = MachineState("mid string", mapping = lambda x : (
            self._string if x == '"' else
            self._in_string))

        # String completion state initialization
        self._string = MachineState("string", StringConst)

        # Add operator state initialization
        self._add_op = MachineState("+ operator", AddOp)

        # Subtraction operator state initialization
        self._sub_op = MachineState("- operator", SubOp)

        # Less than comparator state initialization
        self._lt = MachineState("< comparator", LessThan, mapping = lambda x : (
            self._lte if x == "=" else
            self._ltg if x == ">" else
            None))

        # Less than or equal comparator state initialization
        self._lte = MachineState("<= comparator", LessThanEqual)

        # Less than or greater comparator state initialization
        self._ltg = MachineState("<> comparator", NotEqual)

        # Equality comparator state initialization
        self._eq = MachineState("== comparator", Equal)

        # Greater than comparator state initialization
        self._gt = MachineState("> comparator", GreaterThan,
            mapping = lambda x : (
                self._gte if x == "=" else
                None))

        # Greater than or equal comparator state initialization
        self._gte = MachineState(">= comparator", GreaterThanEqual)

        # Miltiplier operator state initialization
        self._mul_op = MachineState("* operator", MulOp)

        # Division operator state initialization
        self._div_op = MachineState("/ operator", DivOp)

        # Mid-assignment state initialization
        self._in_assign = MachineState(": disambiguator", DeclarationSep,
            mapping = lambda x : ( self._assign if x == "=" else None))

        # Complete assignment state initialization
        self._assign = MachineState("assignment", Assign)

        # Statement separator state initialization
        self._statement_sep = MachineState("statement separator", StatementSep)

        # Left parenthesis state initialization
        self._left_paren = MachineState("found (", LeftParen)

        # Right parenthesis state initialization
        self._right_paren = MachineState("found )", RightParen)

        # Mid-comment state initialization
        self._in_comment = MachineState("mid comment", mapping = lambda x : (
            self._in_comment if x != "}" else
            self._start))

        # Declaration Separator token
        self._declaration_sep = MachineState("declaration separator",
            DeclarationSep)

        # List Separator token
        self._list_sep = MachineState("list separator", ListSeparator)

        # Initialize the HansenLite Lexical Analyzer variables
        self.reset()

    def identifier_disambiguator(self, lexeme):
        """
        Method that disambiguates an identifier lexeme, either returning an
        identifier holding the lexeme, or the given keyword token of the
        language.
        """

        # Get the appropriate token
        token = (
            Program if lexeme == "program" else
            Define if lexeme == "define" else
            Integer if lexeme == "integer" else
            Float if lexeme == "float" else
            String if lexeme == "string" else
            Print if lexeme == "print" else
            Read if lexeme == "read" else
            If if lexeme == "if" else
            On if lexeme == "on" else
            As if lexeme == "as" else
            Then if lexeme == "then" else
            Else if lexeme == "else" else
            While if lexeme == "while" else
            Do if lexeme == "do" else
            Begin if lexeme == "begin" else
            End if lexeme == "end" else
            Identifier
        )

        # Return the lexeme wrapped in the token
        return token(lexeme)
