# jreamy17@georgefox.edu
# CSIS 480: Compiler Register Allocator
# Fall 2019

from collections import deque

class RegisterAllocator():
    """
    The generic register allocator. Discriminates between temporary registers
    and variable register.  Will spill tempoarary registers into any variable
    registers that are not in use if needed, else will spill into the stack.
    """

    # Minimum number of registers free for the register reload to occur
    MIN_TMP_RELOAD = 2

    def __init__(self, tmp_regs, var_regs, out, float=False):
        """
        Initilizes a new Register Allocator.

        Arguments:
        tmp_regs -- the temporary registers that are to be used.
        var_regs -- the variable registers that are to be used.
        out -- the output architecture filewriter.

        Keyword Arguments:
        float -- boolean whether the register allocator is for floats.
            (default=False)
        """

        # Store copies of the temporary and variable register names.
        self.TMP_REGS = tmp_regs.copy()
        self.VAR_REGS = var_regs.copy()

        # Variable registers and memory location
        self.__mem_states = deque()
        self.__reg_of_var = {}
        self.__mem_of_var = {}
        self.__var_edited = []
        self.__var_inited = []

        # Variable priority stacks
        self.__var_priority = deque()
        self.__tmp_priority = deque()
        self.virtual_tmps = 0
        self.tmp_frozen = False

        # Copy the set of free registers
        self.__free_var_regs = var_regs.copy()
        self.__free_tmp_regs = tmp_regs.copy()

        # Initialize the register state stack.
        self.__reg_states = deque()

        # Store the output file writer
        self.out = out
        self.regs_used = deque()

        self.__float = float

    def __repr__(self):
        """
        Returns a string representation of the register allocator listing all
        of the registers in the RegisterAllocator.
        """

        return "RegisterAllocator({}, {})".format(self.TMP_REGS, self.VAR_REGS)

    def get_var_reg(self, var):
        """
        Returns a variable register for the given variable.  Either loads the
        varible into a new register, or returns the register already holding
        the variable.

        Arguments:
        var -- the variable whose register to get.

        Returns:
        A variable register holding the input variable.
        """

        # Get the register if there is a live one
        if var in self.__reg_of_var:
            reg = self.__reg_of_var[var]

        # Get a new register if there is a free one
        elif self.has_free_vars():
            reg = self.__free_var_regs.pop()
            self.load_var(var, reg=reg)

        # Clear a register if needed, and load the value into that one
        else:
            reg = self.store_var(self.__var_priority[0])
            self.load_var(var, reg=reg)

        # Return the register for the variable
        if not reg in self.regs_used:
            self.regs_used.append(reg)
        return reg

    def get_regs_used(self):
        """
        Returns the registers used (in sorted order) since the last call to
        clear_regs_used.  Used for functions storing register before and after
        call.
        """

        regs = []
        if len(self.regs_used) > 0:
            pre = self.regs_used[0][0]
            regs = list(map(lambda x : int(x[1:]), self.regs_used))
            regs = sorted(regs)
            regs = list(map(lambda x : pre + str(x), regs))
        return regs

    def clear_regs_used(self):
        """
        Resets all registers to an 'unused' state.
        """

        self.regs_used.clear()

    def freeze_tmp_stack(self):
        """
        Freezes the temporary registers so that they aren't popped when calling
        a function.
        """

        self.tmp_frozen = True

    def unfreeze_tmp_stack(self):
        """
        Unfreezes the temporary registers so that they are popped when calling
        a function.
        """

        self.tmp_frozen = False

    def get_tmp_reg(self, reg=None):
        """
        Returns a free temp register.  Pushes down the temp stack if needed.

        Keyword Arguments:
        reg -- optional argument holding the register to clear. (default=None)

        Returns:
        A temp register free for assigning.
        """

        # If the temp register is specified, clear that one.
        if reg is not None:
            if reg in self.__free_tmp_regs:
                self.__free_tmp_regs.remove(reg)
            else:
                tmp = self.get_tmp_reg()
                self.pop_tmp_reg(reg=tmp)
                self.out.mov(tmp, reg, float=self.__float)

                idx = self.__tmp_priority.index(reg)
                self.__tmp_priority.remove(reg)
                self.__tmp_priority.insert(idx, tmp)

        # Get a new tmp register if there is a free one
        elif self.has_free_tmps():
            reg = self.__free_tmp_regs.pop(0)

        # Get a new variable register if there is a free one
        elif self.has_free_vars():
            reg = self.__free_var_regs.pop(0)

        # Push down the 'bottom' register of the temp reg 'stack' onto the
        #  actual stack.  Increment depth of temps in the stack.
        else:
            self.push_tmp_dow()
            self.virtual_tmps += 1

        # Set the register to the top of the temp stack
        self.__tmp_priority.append(reg)

        # Return the register
        return reg

    def push_tmp_down(self):
        """
        Pushed down the virtual temp stack into the real stack.
        """

        if len(self.__tmp_priority) > 0:
            reg = self.__tmp_priority.popleft()
            self.out.push(reg, float=self.__float)
        return len(self.__tmp_priority) > 0

    def pop_tmp_reg(self, reg=None):
        """
        Returns the temp register holding the top of the temp 'stack'. Reloads
        the 'bottom' of the temp register stack if there are excess temp
        values on the real stack.

        Keyword Arguments:
        reg -- optional argument holding the register to pop. (default=None)

        Returns:
        The register holding the temp value.
        """

        # Get the register on the top of the stack
        reg = self.__tmp_priority[-1] if reg is None else reg

        if reg in self.TMP_REGS:
            # Refill the temp register stack
            self.reload_tmp_regs()

            # Clear the register from the priority buffer and set as free
            self.__free_tmp_regs.append(reg)
            self.__tmp_priority.remove(reg)

        elif reg in self.VAR_REGS:
            # Clear the register from the priority buffer and set as free
            self.__free_var_regs.append(reg)
            self.__tmp_priority.remove(reg)

        # Return the register
        return reg

    def reload_tmp_regs(self):
        """
        Reloads the temp registers popping off the stack.
        """

        while (len(self.__free_tmp_regs) > RegisterAllocator.MIN_TMP_RELOAD and
                self.virtual_tmps > 0) and not self.tmp_frozen:
            # Get a free temp register and reload it
            reload = self.__free_tmp_regs.pop(0)
            self.__tmp_priority.appendleft(reload)
            self.out.pop(reload, float=self.__float)

            # Decrement the stack depth
            self.virtual_tmps -= 1

    def push_all_tmps(self):
        """
        Pushes all the temp registers onto the real stack.
        """

        for tmp in self.__tmp_priority:
            self.out.push(tmp, float=self.__float)
            self.virtual_tmps += 1

    def clear_all_tmps(self):
        """
        Resets the internal representation of the temp registers, setting them
        all available for use.
        """

        # Clear all tmps regs
        self.__tmp_priority.clear()
        self.__free_tmp_regs = self.TMP_REGS.copy()

    def edit_var(self, var):
        """
        Marks the given variable as edited.

        Arguments:
        var -- the variable to mark as edited.
        """

        self.__var_edited.append(var)

    def init_var(self, var):
        """
        Marks the given variable as initialized.

        Arguments:
        var -- the variable to mark as initialized.
        """

        self.__var_inited.append(var)

    def init_all(self, scope):
        """
        Marks all variables of a given scope as initialized.  Useful for
        entering a while or if block.

        Arguments:
        scope -- the scope of variable names to mark as initialized.
        """

        for var in self.__mem_of_var:
            if scope == var[:len(scope)]:
                self.init_var(var)

    def load_var(self, var, reg=None, force=False):
        """
        Loads the value of the given variable from memory into a variable
        register.

        Arguments:
        var -- the variable to load from memory.

        Keyword Arguments:
        reg -- an optional register argument allowing one to specify the
            register to load the variable into. (default=None)
        force -- whether to force reloading of the variable. (default=False)
        """

        # Only load the variable if it's not in a register
        if not self.has_var_reg(var):
            # Get a free register if one was not specified
            reg = reg if reg is not None else self.get_var_reg(var)

            # Add the variable, register pair to the backing data
            self.__reg_of_var[var] = reg
            self.__var_priority.append(var)

            # Get the memory location of the variable
            if self.has_mem_loc(var):
                mem = self.__mem_of_var[var]
            else:
                self.raise_declaration_error(var)

            # Only load the data if the variable was already initialized
            if self.was_inited(var) or force:
                self.out.ldr(reg, "[FP, #-{}]", mem, float=self.__float)
                self.out.comment("load {} : {}", reg, var)

        elif force:
            self.out.ldr(reg, "[FP, #-{}]", mem, float=self.__float)
            self.out.comment("load {} : {}", reg, var)

    def store_var(self, var):
        """
        Stores the variable from the register into memory and releases the
        register.

        Arguments:
        var -- the variable to store to memory
        """

        # Default the register to None
        reg = None

        if self.has_var_reg(var):
            # Get the register holding the variable and remove it
            reg = self.__reg_of_var.pop(var)
            self.__var_priority.remove(var)

            # Get the memory location of the variable
            if self.has_mem_loc(var):
                mem = self.__mem_of_var[var]
            else:
                self.raise_declaration_error()

            # Store it to memory if the variable was edited
            if self.was_edited(var):
                self.out.str(reg, "[FP, #-{}]", mem, float=self.__float)
                self.out.comment("store {} : {}", var, reg)
                self.__var_edited.remove(var)

        # Return the register
        return reg

    def has_free_vars(self):
        """
        Return whether there are free variable registers.

        Returns:
        True if there are any free variable registers.
        """

        return len(self.__free_var_regs) > 0

    def has_free_tmps(self):
        """
        Return whether there are free temp registers.

        Returns:
        True if there are any free temp registers.
        """

        return len(self.__free_tmp_regs) > 0

    def push_reg_state(self, depth=0):
        """
        Pushes the current register state into the register state stack. For
        use with block boundaries.

        Arguments:
        depth -- the depth to push into the register state stack.  Allows the
        current register state to be pushed into the stack deeper than just the
        top.
        """

        self.__reg_states.rotate(depth)
        self.__reg_states.append((self.__reg_of_var.copy(),
            self.__var_priority.copy(), self.__free_tmp_regs.copy(),
            self.__free_var_regs.copy()))
        self.__reg_states.rotate(-depth)

    def resume_reg_state(self):
        """
        Pops and resumes the register state that is on the top of the register
        state stack.  Useful after exiting blocks where comparisons were the
        last event before exiting.
        """

        (self.__reg_of_var, self.__var_priority, self.__free_tmp_regs,
            self.__free_var_regs) = self.__reg_states.pop()
        self.out.comment("resume {}", self.__reg_of_var)

    def pop_reg_state(self, restore=True):
        """
        Pops and restores the register state that is on the top of the register
        state stack.  Stores any variables that were not previously in use,
        then loads variables that were.

        Keyword Arguments:
        restore -- whether to restore the state or discard it.
        """

        # Get the previous register state
        old_reg_state, old_priority, _, _ = self.__reg_states.pop()

        if restore:
            # Store any variables that were not active before
            for var in self.__reg_of_var.copy():
                # Store the variable if it wasn't in the old state
                if var not in old_reg_state:
                    self.store_var(var)

                # Or store it if it was in a different register
                elif self.__reg_of_var[var] != old_reg_state[var]:
                    self.store_var(var)

            # Load any variables that were active before
            for var in old_reg_state.keys():
                # Load the variable if isn't in the current state
                if var not in self.__reg_of_var:
                    self.load_var(var, reg=old_reg_state[var])

                # Or load the variable if it was in a different register
                elif old_reg_state[var] != self.__reg_of_var[var]:
                    self.load_var(var, reg=old_reg_state[var])

            self.__var_priority = old_priority

    def reg_active(self, reg):
        """
        Returns whether the register is active.

        Arguments:
        reg -- the register to check.

        Returns:
        True if the register has a live value in it
        """

        return ((reg not in self.__free_var_regs) and
                (reg not in self.__free_tmp_regs))

    def get_active_tmps(self):
        """
        Returns the active temp registers.

        Returns:
        A sorted deque of the current active temp registers.
        """

        return sorted(self.__tmp_priority)

    def has_var_reg(self, var):
        """
        Returns whether the variable is in a register.

        Arguments:
        var -- the variable to check.

        Returns:
        True if the variable is currently stored in a register.
        """

        return var in self.__var_priority

    def was_edited(self, var):
        """
        Returns whether the variable was edited while in a register.

        Arguments:
        var -- the variable to check.

        Returns:
        True if the variable was edited while in the current register.
        """

        return var in self.__var_edited

    def was_inited(self, var):
        """
        Returns whether the variable was initialized while in a register.

        Arguments:
        var -- the variable to check.

        Returns:
        True if the variable was initialized previously.
        """

        return var in self.__var_inited

    def has_tmp_reg(self, operand):
        """
        Returns whether the operand register is a live temp register.

        Arguments:
        operand -- the operand register to check.

        Returns:
        True if the temp register is a live register.
        """

        return operand in self.__tmp_priority

    def set_mem_loc(self, var, loc):
        """
        Sets the memory location of the given variable.

        Arguments:
        var -- the variable whose memory location to set
        loc -- the location in memory to set for the variable
        """

        self.__mem_of_var[var] = loc

    def has_mem_loc(self, var):
        """
        Returns whether the variable has a memory location yet.

        Arguments:
        var -- the variable to check.

        Returns:
        True if the variable has been assigned a memory location.
        """

        return var in self.__mem_of_var

    def get_mem_loc(self, var):
        """
        Returns the memory location of the given variable.

        Arguments:
        var -- the variable whose memory location to return.

        Returns:
        The memory location of the variable (stack relative).
        """
        return self.__mem_of_var[var]

    def push_mem_state(self):
        """
        Pushes the current memory allocation to the internal stack.
        """

        self.__mem_states.append(self.__mem_of_var.copy())

    def pop_mem_state(self):
        """
        Restore the memory allocation off the top of the internal stack.
        """

        self.__mem_of_var = self.__mem_states.pop()

    def raise_declaration_error(self, var):
        """
        Raises a SyntaxError requiring the user to declare variables before use.

        Raises:
        SyntaxError -- States that variables must be declared before use.
        """

        raise SyntaxError(("Declaration required before use. Scoped variable"
            " '{}' undeclared.").format(var))
