# jreamy17@georgefox.edu
# CSIS 480: Compiler Non-Terminals
# Fall 2019

from tokens import *
from actions import *

class Script(Symbol):
    """
    The Script non-terminal.  Is a singleton.
    """

    # Define the mapping function of the non-terminal
    _mapping = lambda token : (
        [Program, Identifier, ProgramHeader, FunctionDeclarations, MainHeader,
                VariableDeclarations, Begin, StatementList, End, MainFooter,
                ProgramFooter]
            if isinstance(token, Program) else
        None
    )

    # Define the first set of the non-terminal
    _first = [Program]

class FunctionDeclarations(Symbol):
    """
    The Function Declaration non-terminal.  Is a singleton.
    """

    # Define the mapping function of the non-terminal
    _mapping = lambda token : (
        [Define, Declaration, DeclareFunction, FormalParamList, As,
            DeclareParams, VariableDeclarations, WriteFunction, Begin,
            StatementList, End, FunctionFooter, FunctionDeclarations]
            if isinstance(token, Define) else
        []
            if isinstance(token, (Identifier, Begin)) else
        None
    )

    # Define the first set of the non-terminal
    _first = [Define, Identifier, Begin]

class FormalParamList(Symbol):
    """
    The Formal Parameter List non-terminal.  Is a singleton.
    """

    # Define the mapping function of the non-terminal
    _mapping = lambda token : (
        [On, Declaration, DeclareVariable, InitParam, SeparatedFormalParamList]
            if isinstance(token, On) else
        []
            if isinstance(token, As) else
        None
    )

    # Define the first set of the non-terminal
    _first = [On, As]

class SeparatedFormalParamList(Symbol):
    """
    The Separated Formal Parameter List non-terminal.  Is a singleton.
    """

    # Define the mapping function of the non-terminal
    _mapping = lambda token : (
        [ListSeparator, Declaration, DeclareVariable, InitParam,
                SeparatedFormalParamList]
            if isinstance(token, ListSeparator) else
        []
            if isinstance(token, As) else
        None
    )

    # Define the first set of the non-terminal
    _first = [ListSeparator, As]

class VariableDeclarations(Symbol):
    """
    The Variable Declarations Symbol which begins the variable declaration
    section.
    """

    # Define the mapping function of the non-terminal
    _mapping = lambda token : (
        [Declaration, DeclareVariable, VariableDeclarationList]
            if isinstance(token, Identifier) else
        []
            if isinstance(token, Begin) else
        None
    )

    # Define the first set of the non-terminal
    _first = [Identifier, End, EndOfFile]

class VariableDeclarationList(Symbol):
    """
    The Variable Declaration List non-terminal.  Is a singleton.
    """

    # Define the mapping function of the non-terminal
    _mapping = lambda token : (
        [StatementSep, Identifier, DeclarationSep, VariableType,
                DeclareVariable, VariableDeclarationList]
            if isinstance(token, StatementSep) else
        [DeclareVariableList]
            if isinstance(token, Begin) else
        None
    )

    # Define the first set of the non-terminal
    _first = [StatementSep, Begin]

class Declaration(Symbol):
    """
    The Declaration token, which handles a single variable declaration.
    """

    # Define the mapping function of the non-terminal
    _mapping = lambda token : (
        [Identifier, DeclarationSep, VariableType]
            if isinstance(token, Identifier) else
        None
    )

    # Define the first set of the non-terminal
    _first = [Identifier]

class VariableType(Symbol):
    """
    The Variable Type, used to disambiguate types.  Is a singleton
    """

    # Define the mapping function of the non-terminal
    _mapping = lambda token : (
        [Integer]
            if isinstance(token, Integer) else
        [Float]
            if isinstance(token, Float) else
        [String]
            if isinstance(token, String) else
        None
    )

    # Define the first set of the non-terminal
    _first = [Integer, Float, String]

class Number(Symbol):
    """
    The Number non-terminal (Integer or Float).  Is a singleton.
    """

    # Define the mapping function of the non-terminal
    _mapping = lambda token : (
        [IntegerConst]
            if isinstance(token, IntegerConst) else
        [FloatConst]
            if isinstance(token, FloatConst) else
        None
    )

    # Define the first set of the non-terminal
    _first = [IntegerConst, FloatConst]

class Statement(Symbol):
    """
    The Statement non-terminal.  Is a singleton.
    """

    # Define the mapping function of the non-terminal
    _mapping = lambda token : (
        [Identifier, AssignmentId, Assign, Expression, EvalTerm,
                AssignVariable]
            if isinstance(token, Identifier) else
        [If, BooleanExpression, Then, Statement, ElseClause]
            if isinstance(token, If) else
        [While, BooleanExpression, Do, Statement, EndWhile]
            if isinstance(token, While) else
        [Print, PrintExpression]
            if isinstance(token, Print) else
        [Begin, StatementList, End]
            if isinstance(token, Begin) else
        [Read, Identifier, ScanInput]
            if isinstance(token, Read) else
        None
    )

    # Define the first set of the non-terminal
    _first = [Identifier, If, While, Print, Begin, Read]

class ElseClause(Symbol):
    """
    The ElseClause non-terminal.  Is a singleton.
    """

    # Define the mapping function of the non-terminal
    _mapping = lambda token : (
        [StartElse, Else, Statement, EndElse]
            if isinstance(token, Else) else
        [SkipElse]
            if isinstance(token, (EndOfFile, End, StatementSep)) else
        None
    )

    # Define the first set of the non-terminal
    _first = [EndOfFile, Else, StatementSep]

class StatementList(Symbol):
    """
    The StatementList non-terminal.  Is a singleton.
    """

    # Define the mapping function of the non-terminal
    _mapping = lambda token : (
        [Statement, SeparatedList]
            if isinstance(token, (Identifier, If, While, Print, Begin,
                Read)) else
        None
    )

    # Define the first set of the non-terminal
    _first = [Identifier, If, While, Print, Begin, Read]

class SeparatedList(Symbol):
    """
    The SeparatedList non-terminal.  Is a singleton.
    """

    # Define the mapping function of the non-terminal
    _mapping = lambda token : (
        [StatementSep, Statement, SeparatedList]
            if isinstance(token, StatementSep) else
        []
            if isinstance(token, End) else
        None
    )

    # Define the first set of the non-terminal
    _first = [StatementSep, End]

class PrintExpression(Symbol):
    """
    The PrintExpression non-terminal.  Is a singleton.
    """

    # Define the mapping function of the non-terminal
    _mapping = lambda token : (
        [Expression, EvalTerm, FormatPrintExpression]
            if isinstance(token, (AddOp, LeftParen, Identifier, Constant)) else
        None
    )

    # Define the first set of the non-terminal
    _first = [AddOp, LeftParen, Identifier, DataType]

class Expression(Symbol):
    """
    The Expression non-terminal.  Is a singleton.
    """

    # Define the mapping function of the non-terminal
    _mapping = lambda token : (
        [MathExpression]
            if isinstance(token, (AddOp, LeftParen, Identifier,
                IntegerConst, FloatConst)) else
        [PushPostfix, StringConst]
            if isinstance(token, StringConst) else
        None
    )

    # Define the first set of the non-terminal
    _first = (AddOp, LeftParen, Identifier, IntegerConst, FloatConst,
        StringConst)

class MathExpression(Symbol):
    """
    The Math Expression non-terminal.  Is a singleton.
    """

    # Define the mapping function of the non-terminal
    _mapping = lambda token : (
        [Term, Addition]
            if isinstance(token, (AddOp, LeftParen, Identifier,
                IntegerConst, FloatConst)) else
        None
    )

    # Define the first set of the non-terminal
    _first = [AddOp, LeftParen, Identifier, IntegerConst, FloatConst]

class Addition(Symbol):
    """
    The Addition non-terminal.  Is a singleton.
    """

    # Define the mapping function of the non-terminal
    _mapping = lambda token : (
        [PushOperator, AddOp, Term, Addition]
            if isinstance(token, (AddOp)) else
        []
            if isinstance(token, (RightParen, EndOfFile, End, Else,
                StatementSep, ListSeparator)) else
        None
    )

    # Define the first set of the non-terminal
    _first = [AddOp, RightParen, EndOfFile, End, StatementSep, Else]

class Term(Symbol):
    """
    The Term non-terminal.  Is a singleton.
    """

    # Define the mapping function of the non-terminal
    _mapping = lambda token : (
        [Factor, Multiplication]
            if isinstance(token, (AddOp, LeftParen, Identifier,
                IntegerConst, FloatConst)) else
        None
    )

    # Define the first set of the non-terminal
    _first = [AddOp, LeftParen, Identifier, IntegerConst, FloatConst]

class Multiplication(Symbol):
    """
    The Multiplication non-terminal.  Is a singleton.
    """

    # Define the mapping function of the non-terminal
    _mapping = lambda token : (
        [PushOperator, MulOp, Factor, Multiplication]
            if isinstance(token, MulOp) else
        []
            if isinstance(token, (AddOp, RightParen, EndOfFile, End,
                StatementSep, Else, ListSeparator)) else
        None
    )

    # Define the first set of the non-terminal
    _first = [MulOp, AddOp, RightParen, EndOfFile, End, Else,
            StatementSep]

class BooleanExpression(Symbol):
    """
    The BooleanExpression non-terminal.  Is a singleton.
    """

    # Define the mapping function of the non-terminal
    _mapping = lambda token : (
        [Factor, PushOperator, Comparator, Factor, EvalTerm]
            if isinstance(token, (AddOp, LeftParen, Identifier,
                IntegerConst, FloatConst)) else
        None
    )

    # Define the first set of the non-terminal
    _first = [AddOp, LeftParen, Identifier, IntegerConst, FloatConst]

class SignedTerm(Symbol):
    """
    The SignedTerm non-terminal.  Is a singleton.
    """

    # Define the mapping function of the non-terminal
    _mapping = lambda token : (
        [EvalSign, AddOp, Term]
            if isinstance(token, AddOp) else
        None
    )

    # Define the first set of the non-terminal
    _first = [AddOp]

class Factor(Symbol):
    """
    The Factor non-terminal.  Is a singleton.
    """

    # Define the mapping function of the non-terminal
    _mapping = lambda token : (
        [PushOperator, LeftParen, MathExpression, PushOperator, RightParen]
            if isinstance(token, LeftParen) else
        [PushPostfix, Identifier, Parameters]
            if isinstance(token, Identifier) else
        [PushPostfix, Number]
            if isinstance(token, (IntegerConst, FloatConst)) else
        [SignedTerm]
            if isinstance(token, AddOp) else
        None
    )

    # Define the first set of the non-terminal
    _first = [LeftParen, Identifier, IntegerConst, FloatConst, AddOp]

class Parameters(Symbol):
    """
    The Parameters non-terminal.  Is a singleton.
    """

    # Define the mapping function of the non-terminal
    _mapping = lambda token : (
        [Function, BeginParam, LeftParen, ParamList, RightParen, EvalFunction]
            if isinstance(token, LeftParen) else
        []
            if isinstance(token, (MulOp, AddOp, RightParen, StatementSep, End,
                Comparator, ListSeparator, Then, Else)) else
        None
    )

    # Define the first set of the non-terminal
    _first = [LeftParen, MulOp, AddOp, RightParen, StatementSep, End,
        Comparator, ListSeparator, Then, Else]

class ParamList(Symbol):
    """
    The Parameter List non-terminal.  Is a singleton.
    """

    # Define the mapping function of the non-terminal
    _mapping = lambda token : (
        [Expression, EvalParam, BeginParam, SeparatedParamList]
            if isinstance(token, (AddOp, LeftParen, Identifier, IntegerConst,
                FloatConst, StringConst)) else
        []
            if isinstance(token, RightParen) else
        None
    )

    # Define the first set of the non-terminal
    _first = [AddOp, LeftParen, Identifier, IntegerConst, FloatConst,
        StringConst, RightParen]

class SeparatedParamList(Symbol):
    """
    The Separated Parameter List non-terminal.  Is a singleton.
    """

    # Define the mapping function of the non-terminal
    _mapping = lambda token : (
        [ListSeparator, Expression, EvalParam, BeginParam, SeparatedParamList]
            if isinstance(token, ListSeparator) else
        []
            if isinstance(token, RightParen) else
        None
    )

    # Define the first set of the non-terminal
    _first = [ListSeparator, RightParen]
