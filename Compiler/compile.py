# jreamy17@georgefox.edu
# CSIS 480: Compiler Call Script
# Fall 2019

from compiler import Compiler
import argparse

import os

if __name__ == "__main__":
    # Set up command line parser
    ap = argparse.ArgumentParser()
    ap.add_argument("-f", "--file", default="input1.h",
        help="file name")
    ap.add_argument("-o", "--out", default="output.s",
        help="file name for output")
    ap.add_argument("-a", "--all", action="store_true",
        help="whether to compile all the files in the input path")
    ap.add_argument("-s", "--show", action="store_true",
        help="whether to print all terminals and non-terminals during parse")
    args = vars(ap.parse_args())


    # Allow compiling all files in a source directory to an output directory
    if (args["all"]):
        for file in os.listdir(args["file"]):
            # Set up the Compiler
            compiler = Compiler()

            # Get the output file name
            outfile = file[:-3] + ".s"

            # Get file names
            input = os.path.join(args["file"], file)
            output = os.path.join(args["out"], outfile)

            # Compile files
            print("$ python compile.py -f {} -o {}".format(file, outfile))
            try:
                # Try compiling
                compiler.compile(input, output, show_all=args["show"])
            except SyntaxError as error:
                # Print the error if failed
                print("SyntaxError:", error)

            print()

    else:
        # Set up the Compiler
        compiler = Compiler()

        # Perform the compilation
        compiler.compile(args["file"], args["out"], show_all=args["show"])
