# jreamy17@georgefox.edu
# CSIS 480: Compiler VirtualMachine
# Fall 2019

from register_allocator import RegisterAllocator
from tokens import Integer, IntegerConst, Float, FloatConst, String, StringConst, ListSeparator
from collections import deque
import ctypes

def peek(deque):
    """
    Peek function to return the top of the deque or None if the deque is empty.

    Arguments:
    deque -- the deque to return the top of.

    Returns:
    The top of the deque or None if the deque is empty.
    """

    return None if len(deque) == 0 else deque[-1]

class VirtualMachine():
    """
    The Virtual Machine in charge of register allocation.
    """

    def __init__(self, compiler):
        """
        Initializes a new Virtual Machine.

        Arguments:
        compiler -- the compiler that the Virtual Machine is connected to.
        """

        # Define the temp and variable registers
        self.tmp_int_regs = ["R" + str(x) for x in range(0, 4)]
        self.var_int_regs = ["R" + str(x) for x in range(4, 11)]
        self.tmp_float_regs = ["S" + str(x) for x in range(0, 8)]
        self.var_float_regs = ["S" + str(x) for x in range(8, 31)]

        # Get the register allocator for int variables
        self.regs = RegisterAllocator(self.tmp_int_regs, self.var_int_regs,
            compiler.out)

        # Get the register allocator for float variables
        self.fregs = RegisterAllocator(self.tmp_float_regs,
            self.var_float_regs, compiler.out, float=True)

        # Store the compiler and the output architecture file writer
        self.master = compiler
        self.out = compiler.out

        # Internal stack and queues for the postfix conversion and computation
        self.postfix = deque()
        self.__postfix = self.postfix
        self.operators = deque()
        self.__operators = self.operators
        self.operands = deque()
        self.__operands = self.operands

        # Function call stack for function nesting in parameter evaluations
        self.call_stack = deque()

        # Conditional label stack
        self.labels = deque()
        self.num_labels = {}

        # Default the scope and scope stack
        self.__scope_stack = deque()
        self.__scope = "main"
        self.num_vars = 0
        self.__var_types = {}
        self.undeclared_vars = 0
        self.__function_types = {
            "__aeabi_idiv" : Integer
        }
        self.__function_params = {}
        self.params = deque()

        # Set up the known strings
        self.__prev_string = ""
        self.strings = {
            "\"%s\"":"string_format",
            "\"%d\"":"integer_format",
            "\"%f\"":"float_format"
        }

    def end_param(self):
        """
        Ends the current parameter evaluation.  Returns to the outer function
        call if there is one, or the main vm scope.
        """

        # Move up one layer in function nesting
        if len(self.call_stack) > 0:
            function = self.call_stack[-1]
            self.postfix = function.postfix
            self.operators = function.operators
            self.operands = function.operands

        # Return to main vm scope
        else:
            self.postfix = self.__postfix
            self.operators = self.__operators
            self.operands = self.__operands

    def begin_param(self):
        """
        Begins a parameter evaluation, setting the operand, postfix and
        operator deques as the functions'.
        """

        function = self.call_stack[-1]
        self.postfix = function.postfix
        self.operators = function.operators
        self.operands = function.operands

    def add_label(self, label):
        """
        Adds the label to the backing label memory.

        Arguments:
        label -- the label to add

        Returns:
        the number of labels with the same prefix as the current label
        """

        if label in self.num_labels:
            self.num_labels[label] += 1
        else:
            self.num_labels[label] = 0

        return self.num_labels[label]

    def get_num_labels(self, label):
        """
        Returns the current number of labels in the virtual machine of the same
        label prefix.

        Arguments:
        label -- the label prefix to check

        Returns:
        The number of labels added with the same prefix
        """

        return self.num_labels[label]

    def push_label(self, label):
        """
        Pushes the label onto the label stack.
        """

        self.labels.append(label)

    def pop_label(self):
        """
        Pops the label off the label stack and returns.

        Returns:
        The label from the top of the label stack.
        """

        return self.labels.pop()

    def set_scope(self, scope):
        """
        Sets the variable scope to that given, resets the number of variables

        Arguments:
        scope -- the new scope
        """

        self.__scope = scope
        self.num_vars = 0
        self.undeclared_vars = 0

    def get_scope(self):
        """
        Returns the current scope of the vm.

        Returns:
        The current scope of the vm.
        """

        return self.__scope

    def push_scope(self):
        """
        Pushes the current scope onto the internal scope stack. Allows block
        boundaries to maintain their own scope.  While accessing the scope of
        the block that they're in.
        """

        # Save the current scope
        scope = (self.__scope, self.num_vars, self.undeclared_vars)
        self.__scope_stack.append(scope)

        # Save the current variable memory allocation
        self.regs.push_mem_state()

    def pop_scope(self):
        """
        Pops the current scope off the internal scope stack.  Restores the
        variable scope to what it was before the block boundary.
        """

        # Restore the scope
        scope = self.__scope_stack.pop()
        self.__scope, self.num_vars, self.undeclared_vars = scope

        # restore the memory allocation
        self.regs.pop_mem_state()

    def assert_types(self, op1, op2):
        """
        Asserts that the types of the two operands are the same.

        Raises:
        SyntaxError -- if the types of the operands are not the same.
        """

        if op1[1] != op2[1]:
            raise SyntaxError(("Type Mismatch between {}"
                " and {}\n{}".format(op1, op2,
                self.master.lexer.display_error())))
        elif op1[1] == String:
            raise SyntaxError(("String math not supported\n{}".format(
                self.master.lexer.display_error(backup=1))))

    def add_var(self, var, datatype):
        """
        Adds the given variable to the scope.

        Arguments:
        var -- the variable to add
        datatype -- the variable type
        """

        # Get the scoped variable name
        var_name = self.get_var_name(var)

        # Raise an exception if the variable is already declared
        if var_name in self.__var_types:
            raise SyntaxError(("Variable '{}' on line {} previously"
                " declared".format(var_name, self.master.lexer._line_number-1)))

        # Allocate memory location
        if datatype == Float:
            self.fregs.set_mem_loc(var_name, 4 * self.num_vars)
        else:
            self.regs.set_mem_loc(var_name, 4 * self.num_vars)

        # Set the variable type
        self.__var_types[var_name] = datatype

        # Increment number of variables to declare
        self.num_vars += 1
        self.undeclared_vars += 1

    def init_var(self, var, datatype):
        """
        Initializes the variable in the correct registers given the variable
        and the datatype.

        Arguments:
        var -- the variable to initialize
        datatype -- the variable type
        """

        if datatype == Float:
            self.fregs.init_var(var)
        else:
            self.regs.init_var(var)

    def declare_vars(self):
        """
        Generates code declaring all the variables added since the last
        declaration.
        """

        self.out.sub("SP", "#{}".format(4 * self.undeclared_vars))
        self.out.comment("make room for {} variables",
            self.undeclared_vars)
        self.undeclared_vars = 0

    def get_var_name(self, var):
        """
        Returns the variable name prefixed by the current scope.

        Arguments:
        var -- the variable to prefix

        Returns:
        The variable name prefixed by the current scope
        """

        return self.__scope + "." + var

    def get_var_type(self, var):
        """
        Returns the datatype of the input variable.

        Arguments:
        var -- the variable whose data type to return

        Returns:
        The datatype of the input variable
        """

        return self.__var_types[var] if var in self.__var_types else None

    def init_all_vars(self):
        """
        Initializes all the variables of the current scope.
        """

        self.regs.init_all(self.__scope)
        self.fregs.init_all(self.__scope)

    def add_string(self, string):
        """
        Adds a the given string to the internal dict of strings.

        Arguments:
        string -- the string to add
        """

        if not string in self.strings:
            string_id = "string_{}".format(len(self.strings))
            self.strings[string] = string_id
        self.__prev_string = self.strings[string]

    def get_prev_string(self):
        """
        Returns the most recently added string.

        Returns:
        The most recently added string
        """

        return self.__prev_string


    def add_function(self, function_name, return_type):
        """
        Adds the function to the vm.

        Arguments:
        function_name -- the name of the function to add
        return_type -- the return datatype of the function
        """

        self.__function_types[function_name] = return_type

    def get_function_type(self, function_name):
        """
        Returns the return type of the input function.

        Arguments:
        function_name -- the name of the function whose type to get

        Returns:
        The return datatype of the input function
        """

        return self.__function_types[function_name]

    def set_params(self, function_name, params):
        """
        Adds the function parameters to the vm.

        Arguments:
        function_name -- the name of the function to add
        params -- the parameter list of the function
        """

        self.__function_params[function_name] = params

    def get_params(self, function_name):
        """
        Returns the parameter list of the input function.

        Arguments:
        function_name -- the name of the function whose parameters to return

        Returns:
        The parameter list of the input function
        """

        return self.__function_params[function_name]

    def is_function(self, identifier):
        """
        Returns whether the identifier is a function.

        Arguments:
        identifier -- the identifier to check

        Returns:
        True if the identifier is a function name
        """

        return identifier in self.__function_types

    def call(self, function_name, args=None, rets=1, reg_pass=True,
            double_align=False, condition="L"):
        """
        Sets up and implements a call to the given function.

        Arguments:
        function_name -- the function to call.

        Keyword Arguments:
        args -- the number of arguments to pass to the function. Loads the
            arguments for the given function if None given. (defualt=None)
        rets -- the number of return values from the function. (default=1)
        reg_pass -- whether the function expects values passed in registers.
            (default=True)
        double_align -- whether to double align the stack before function call.
            (default=False)
        condition -- optional condition for branching. (default="L")
        """

        # Default the arguments and returns
        if args is None:
            args = len(self.get_params(function_name))

        if reg_pass:
            self.out.push("LR")

        # Push all tmp values (the actual argument stack)
        self.regs.push_all_tmps()
        self.regs.clear_all_tmps()
        self.out.comment("saved all temp registers")

        if double_align:
            # Align the stack
            self.out.add("R0", "SP", "#{}".format(4*args))
            self.out.three_op("AND", "R0", "R0", "#7")
            self.out.cmp("R0", "#0")
            label_n = str(self.add_label("stack_align"))
            self.out.branch("stack_align_" + label_n, "EQ")
            self.regs.push_reg_state()

        # Pop args into tmp regs
        if reg_pass:
            for _ in range(args):
                self.operands.pop()
                tmp = self.regs.get_tmp_reg()
                self.out.pop(tmp)
                self.regs.virtual_tmps -= 1
            self.out.comment("reloaded temp registers in correct order")

        if double_align:
            self.out.sub("SP", "#4")

        # Call function
        self.out.branch(function_name, condition)

        if double_align:
            self.out.add("SP", "#4")

        if len(self.call_stack) == 0:
            self.regs.unfreeze_tmp_stack()

        # Clear the temp register allocation
        self.regs.clear_all_tmps()

        if double_align:
            self.out.branch("end_align_" + label_n)
            self.out.label("stack_align_" + label_n)
            self.regs.resume_reg_state()

            if reg_pass:
                for _ in range(args):
                    tmp = self.regs.get_tmp_reg()
                    self.out.pop(tmp)
                self.out.comment("reloaded temp registers in correct order")

            # Call function
            self.out.branch(function_name, condition)

            # Clear the temp register allocation
            self.regs.clear_all_tmps()

            self.out.label("end_align_" + label_n)

        # Setup returns
        for x in range(rets):
            datatype = self.__function_types[function_name]

            # Return type float
            if datatype == Float:
                if reg_pass:
                    reg = "S"+str(x)
                else:
                    reg = self.fregs.get_tmp_reg()
                    self.out.pop(reg, float=True)
                var = (reg, Float)

            # Return type integer or string
            elif datatype == Integer or datatype == String:
                if reg_pass:
                    reg = "R"+str(x)
                else:
                    reg = self.regs.get_tmp_reg()
                    self.out.pop(reg)
                var = (reg, datatype)

            self.push_operand(var)

        self.out.pop("LR")


    def push_operand(self, operand, load_tmp=False):
        """
        Pushes the given operand into the operand stack.

        Arguments:
        operand -- the operand to push onto the stack.

        Keyword Arguments:
        load_tmp -- whether to load the operand into a temp register before
        pushing onto the operand stack. (default=False)
        """

        if load_tmp:
            # Load the value of operand into the temp stack
            if operand[1] == Integer:
                tmp = self.regs.get_tmp_reg()
                self.out.mov(tmp, operand[0])
                self.operands.append(("RX", Integer))
            elif operand[1] == Float:
                tmp = self.fregs.get_tmp_reg()
                self.out.mov(tmp, operand[0], float=True)
                self.operands.append(("SX", Float))
            elif operand[1] == String:
                tmp = self.regs.get_tmp_reg()
                self.out.mov(tmp, operand[0])
                self.operands.append(("RX", String))
            else:
                self.operands.append(operand)
        elif self.regs.has_tmp_reg(operand[0]):
            # If it's already a register in the temp stack, deepen the temp
            #  stack
            self.operands.append(("RX", operand[1]))
        elif self.fregs.has_tmp_reg(operand[0]):
            # If it's already in a float register in the temp stack, deepen
            #  that stack
            self.operands.append(("SX", operand[1]))
        else:
            # Otherwise just append the operand
            self.operands.append(operand)

    def pop_operand(self, load_tmp=False):
        """
        Pops and returns the operand from the top of the temp stack.

        Keyword Arguments:
        load_tmp -- whether to load a temporary register with the value.
            (default=False)

        Returns:
        the operand on top of the operand stack.
        """

        # Get the top of the operand stack
        operand = self.operands.pop()

        # Get the temp register if it's a temp
        if operand[0] == "RX":
            operand = (self.regs.pop_tmp_reg(), operand[1])

        elif operand[0] == "SX":
            operand = (self.fregs.pop_tmp_reg(), Float)

        # Load the constant if it's a constant
        elif operand[1] == IntegerConst:
            tmp = self.regs.get_tmp_reg()
            self.out.ldr(tmp, "=" + operand[0])
            operand = (tmp, Integer)
            self.regs.pop_tmp_reg(reg=tmp)

        elif operand[1] == FloatConst:
            tmp = self.regs.get_tmp_reg()
            b = bin(ctypes.c_uint.from_buffer(ctypes.c_float(float(operand[0]))).value)
            lsw = b[:2] + b[-16:]
            msw = b[:2] + "0"*(34-len(b)) + b[2:len(b)-16]
            self.out.three_op("movw", tmp, lsw)
            self.out.three_op("movt", tmp, msw)
            reg = self.fregs.get_tmp_reg()
            self.out.three_op("vmov", reg, tmp)
            self.fregs.pop_tmp_reg(reg=reg)
            self.regs.pop_tmp_reg(reg=tmp)

            operand = (reg, Float)

        elif operand[1] == StringConst:
            tmp = self.regs.get_tmp_reg()
            self.out.ldr(tmp, "=" + self.strings[operand[0]])
            self.regs.pop_tmp_reg(reg=tmp)
            operand = (tmp, String)

            """
            # TODO: Remove
            """

        # Get the variable register if it's a variable
        elif self.regs.has_mem_loc(operand[0]):
            operand = (self.regs.get_var_reg(operand[0]), operand[1])
            if load_tmp:
                tmp = self.regs.get_tmp_reg()
                self.regs.pop_tmp_reg(reg=tmp)
                self.out.mov(tmp, operand[0])
                operand = (tmp, operand[1])

        elif self.fregs.has_mem_loc(operand[0]):
            operand = (self.fregs.get_var_reg(operand[0]), operand[1])
            if load_tmp:
                tmp = self.fregs.get_tmp_reg()
                self.fregs.pop_tmp_reg(reg=tmp)
                self.out.mov(tmp, operand[0], float=True)
                operand = (tmp, operand[1])

        # Return the operand
        return operand

    def pop_operands(self):
        """
        Pops and returns the operands on the top of the stack in the format:
        destination, source 1, source 2.

        Returns:
        dest -- the destination register
        src1 -- the first source register
        src2 -- the second sourc register
        """

        # Get the top two operands
        src1 = self.pop_operand()
        src2 = self.pop_operand()

        # Set the destination to one of the sources if it's a temp register
        if self.regs.has_tmp_reg(src1[0]) or self.fregs.has_tmp_reg(src1[0]):
            dest = src1
        elif self.regs.has_tmp_reg(src2[0]) or self.fregs.has_tmp_reg(src2[0]):
            dest = src2

        # Otherwise get a new temp register
        elif src1[0] in self.tmp_int_regs or src1[0] in self.var_int_regs:
            dest = (self.regs.get_tmp_reg(), src1[1])
        elif src1[0] in self.tmp_float_regs or src1[0] in self.var_float_regs:
            dest = (self.fregs.get_tmp_reg(), src1[1])
        elif src2[0] in self.tmp_int_regs or src2[0] in self.var_int_regs:
            dest = (self.regs.get_tmp_reg(), src2[1])
        elif src2[0] in self.tmp_float_regs or src2[0] in self.var_float_regs:
            dest = (self.fregs.get_tmp_reg(), src2[1])

        # Return the registers
        return dest, src1, src2

    def push_postfix(self, token):
        """
        Pushes the given token to the postfix stack.

        Arguments:
        token -- the token to push to the postfix stack.
        """

        self.postfix.append(token)

    def push_operator(self, token):
        """
        Pushes the given token to the operator stack.

        Arguments:
        token -- the token to push to the operator stack.
        """

        # Check for parentheses
        if token.max_precedence():
            # If left paren, push to operator stack
            if token.begin_push():
                self.operators.append(token)

            # If right paren, pop things from operator stack onto postfix stack
            else:
                while len(self.operators) > 0:
                    # Get the operator
                    operator = self.operators.pop()

                    # Exit the loop if hit other parenthesis
                    if operator.max_precedence():
                        break

                    # Otherwise push the operator
                    self.push_postfix(operator)

        # If it's not a parenthesis, push to operator stack
        else:
            # If the top of the operator stack is lower precedence than the
            #  current token, push the following operators onto the postfix
            #  stack.
            if not token.has_precedence(peek(self.operators)):

                # Push operators onto the stack until a parenthesis is found
                while (len(self.operators) > 0
                        and not self.operators[-1].max_precedence()):
                    self.push_postfix(self.operators.pop())

            # Push the current token onto the operator stack
            self.operators.append(token)

    def eval_postfix(self):
        """
        Evaluates the current arithmetic expression, now in postfix format
        """

        # Load the rest of the operators onto the postfix stack
        while len(self.operators) > 0:
            self.push_postfix(self.operators.pop())

        # Evaluate the entire postfix stack
        while len(self.postfix) > 0:
            symbol = self.postfix.popleft()
            symbol.compute(self.master)
