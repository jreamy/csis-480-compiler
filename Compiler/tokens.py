# jreamy17@georgefox.edu
# CSIS 480: Compiler Tokens (Terminals)
# Fall 2019

class Symbol():
    """
    Generic Symbol class that terminals and non-terminals extend in order to
    be used in the SyntaxAnalyzer.  Allows subclasses to be declared as either
    singleton classes, in which case get_instance will return the singleton,
    otherwise get_instance will return a new instance of the subclass.
    Subclasses can also be declared to be terminal or not, which determines
    their usage in the SyntaxAnalyzer.
    """

    _singleton = True
    _instance = None
    _terminal = False
    _non_terminal = True
    _action = False

    _mapping = lambda x : None

    @classmethod
    def get_instance(cls, *args):
        """
        Returns the singleton object if the class is a singleton, else a new
        instance of the class.

        Arguments:
        *args -- any arguments passed will be passed to the constructor if first
            singleton instantiation, or non-singleton instantiation

        Returns:
        the singleton object of the subclass if the subclass is a singleton,
        else a new instance of the subclass.
        """

        if not cls._singleton or cls._instance is None:
            cls._instance = cls(*args)

        return cls._instance

    @classmethod
    def is_terminal(cls):
        """
        Returns True if the class is a terminal.

        Returns:
        True if the class is a terminal, False otherwise.
        """

        return cls._terminal

    @classmethod
    def is_non_terminal(cls):
        """
        Returns True if the class is a terminal.

        Returns:
        True if the class is a terminal, False otherwise.
        """

        return cls._non_terminal

    @classmethod
    def is_action(cls):
        """
        Returns True if the class is an action.

        Returns:
        True if the class is an action, False otherwise.
        """

        return cls._action

    @classmethod
    def get_first(cls):
        """
        Returns the first set of a non-terminal, or the terminal corrosponging
        to the symbol.

        Returns:
        Either the string representation of the terminal, or the list of
        strings in the non-terminal's first set.
        """

        if not cls._terminal:
            output = list(map(lambda x: x.__name__, cls._first))
        else:
            output = cls

        return output

    @classmethod
    def __getitem__(cls, index):
        """
        Allows indexing of a Symbol instance.

        Arguments:
        index -- a Symbol used to index the mapping given at initialization.

        Returns:
        the list of Symbols corrosponding to the given Symbol.
        """

        return cls._mapping(index)

    def __repr__(self):
        """
        Generates a string representation of the object.

        Returns:
        A string representation of the object.
        """

        return "{}()".format(self.__class__.__name__)

    def __str__(self):
        """
        Generates a string representation of the object.

        Returns:
        A string representation of the object.
        """

        return self.__repr__()


class Token(Symbol):
    """
    Generic callable Token type.  Stores the lexeme if relevant.
    """

    _terminal = True
    _non_terminal = False

    def __init__(self, lexeme=None, function = lambda *a, **k: None):
        """
        Constructs a new Token object.

        Keyword Arguments:
        lexeme -- the lexeme associated to the token.
        """

        super().__init__()
        self.__lexeme = lexeme

    def __str__(self):
        """
        Returns a formatted string representation of the object.
        """

        # Format so that a 'None' lexeme appears as ""
        lex = self.get_lexeme()
        type_name = self.__class__.__name__
        return "{}({})".format(type_name, lex)

    def __eq__(self, other):
        """
        An Equality check based on the lexeme held in each token
        """

        return (isinstance(other, self.__class__) and
            self.get_lexeme() == other.get_lexeme())

    def __hash__(self):
        """
        Allows hashing of the Symbol.

        Returns:
        the hash of the symbol's lexeme.
        """

        return self.__lexeme.__hash__()

    def get_lexeme(self):
        """
        Returns the lexeme of the token.

        Returns:
        The lexeme of the token.
        """

        return self.__lexeme

class Identifier(Token):
    """
    The Identifier token.  Is not a singleton.
    """

    _singleton = False
    _action = True

    def __call__(self, compiler):
        """
        When called, stores itself as the previously encountered lexeme of the
        compiler.

        Arguments:
        compiler -- the compiler on which to act.
        """

        compiler._prev_id = self.get_lexeme()

    def compute(self, compiler):
        """
        When called, pushes itself onto the compiler's operand stack.

        Arguments:
        compiler -- the compiler on which to act.
        """

        # Store the id of the Identifier
        compiler._prev_id = self.get_lexeme()

        # Gets a register for the variable
        variable = compiler.vm.get_var_name(self.get_lexeme())
        datatype = compiler.vm.get_var_type(variable)
        if datatype == Float:
            compiler.vm.fregs.get_var_reg(variable)
        else:
            compiler.vm.regs.get_var_reg(variable)

        # Pushes the identifier to the operand stack
        var = (variable, datatype)
        compiler.vm.push_operand(var)

class Constant(Token):
    """
    The generic Constant token for all constants in the input code.  Is not a
    singleton.
    """

    _singleton = False

    def __repr__(self):
        """
        Returns a string representation of the Constant.
        """

        return self.get_lexeme()

class FloatConst(Constant):
    """
    The Float Constant tokeen.  Is not a singleton.
    """

    def compute(self, compiler):
        """
        When called, loads the number into a temp reg and pushes itself onto
        the operand stack.

        Arguments:
        compiler -- the compiler on which to act.
        """

        compiler.vm.push_operand((self.get_lexeme(), FloatConst))

class IntegerConst(Constant):
    """
    The Integer Constant token.  Is not a singleton.
    """

    def compute(self, compiler):
        """
        When called, loads the number into a temp reg and pushes itself onto
        the operand stack.

        Arguments:
        compiler -- the compiler on which to act.
        """

        compiler.vm.push_operand((self.get_lexeme(), IntegerConst))

class StringConst(Constant):
    """
    The Integer Constant token.  Is not a singleton.
    """

    def compute(self, compiler):
        """
        When called, loads the number into a temp reg and pushes itself onto
        the operand stack.

        Arguments:
        compiler -- the compiler on which to act.
        """

        compiler.vm.add_string(self.get_lexeme())
        compiler.vm.push_operand((self.get_lexeme(), StringConst))

class Program(Token):
    """
    The Program token.
    """

    pass

class Define(Token):
    """
    The Define token.
    """

    pass

class On(Token):
    """
    The On token.
    """

    pass


class As(Token):
    """
    The As token.
    """

    pass

class DeclarationSep(Token):
    """
    The Declaration Separator token.
    """

    pass

class ListSeparator(Token):
    """
    The List Separator token.
    """

    pass

class DataType(Token):
    """
    The generic Data Type token.
    """

    _action = True

    def __call__(self, compiler):
        """
        Stores the current DataType in the compiler as the most recent datatype.

        Arguments:
        compiler -- the compiler in which to store the recent datatype.
        """

        compiler.prev_type = self.__class__

class Integer(DataType):
    """
    The Integer type token.
    """

    pass

class Float(DataType):
    """
    The Float type token.
    """

    pass

class String(DataType):
    """
    The String type token.
    """

    pass

class Print(Token):
    """
    The Print token.
    """

    pass

class Read(Token):
    """
    The Read token.
    """

    pass

class If(Token):
    """
    The If token.
    """

    _action = True

    def __call__(self, compiler):
        """
        When called, pushes the labels for an if else onto the label stack

        Arguments:
        compiler -- the compiler on which to act.
        """

        # Get the current label
        num_ifs = compiler.vm.add_label("if")
        compiler.out.label("start_if_{}", num_ifs)

        # Push the followup labels
        compiler.vm.push_label("endif_{}".format(num_ifs))
        compiler.vm.push_label("else_{}".format(num_ifs))
        compiler.vm.push_label("endif_{}".format(num_ifs))
        compiler.vm.push_label("else_{}".format(num_ifs))


class Then(Token):
    """
    The Then token.
    """

    _action = True

    def __call__(self, compiler):
        """
        When called, pushes the current register state to the compiler's
        register state stack.

        Arguments:
        compiler -- the compiler on which to act.
        """

        # Store register states for use exiting the if block
        compiler.vm.regs.push_reg_state()
        compiler.vm.regs.push_reg_state()
        compiler.vm.regs.push_reg_state()

        compiler.vm.push_scope()
        compiler.vm.push_scope()

class Else(Token):
    """
    The Else token.
    """

    pass

class While(Token):
    """
    The While token.
    """

    _action = True

    def __call__(self, compiler):
        """
        When called, sets up a new while state, getting labels and pushing the
        current register state to the register state stack.

        Arguments:
        compiler -- the compiler on which to act.
        """

        # Get current label
        num_whiles = compiler.vm.add_label("while")
        compiler.out.label("start_while_{}".format(num_whiles))

        # Push the followup labels
        compiler.vm.push_label("end_while_{}".format(num_whiles))
        compiler.vm.push_label("start_while_{}".format(num_whiles))
        compiler.vm.push_label("end_while_{}".format(num_whiles))

        # Store the register state for exiting the while block
        compiler.vm.regs.push_reg_state()
        compiler.vm.init_all_vars()

class Do(Token):
    """
    The Do token.
    """

    _action = True

    def __call__(self, compiler):
        """
        When called, pushes the register state 1 deep into the register state
        stack.

        Arguments:
        compiler -- the compiler on which to act.
        """

        # Push the register state 'under' the top of the stack so it can
        #  be resumed exiting the loop
        compiler.vm.regs.push_reg_state(depth=1)
        compiler.vm.push_scope()


class Begin(Token):
    """
    The Begin token.
    """

    pass

class End(Token):
    """
    The End token.
    """

    pass

class Operator(Token):
    """
    The Operator Token, super to all mathematic operators.
    """

    _priority = 0

    @classmethod
    def has_precedence(cls, other):
        """
        Returns if the current token is higher priority than the input.

        Arguments:
        other -- the token to check against.

        Returns:
        True if the objects priority is higher than the input's.
        """

        return other is not None and (cls._priority > other._priority)

    @classmethod
    def max_precedence(cls):
        """
        Returns whether the current token is a parenthesis.

        Returns:
        True if the current token is a parenthesis
        """

        return cls._priority >= 3

    @classmethod
    def begin_push(cls):
        """
        Returns whether the current token is a parenthesis.

        Returns:
        True if the current token is a left parenthesis
        """

        return cls._priority == 4

class MulOp(Operator):
    """
    The Multiplication Operator token.
    """

    _priority = 2

    def compute(self, compiler):
        """
        When called, generates a multiplication operation in the output.

        Arguments:
        compiler -- the compiler on which to act.
        """

        dest, src1, src2 = compiler.vm.pop_operands()
        compiler.vm.assert_types(src1, src2)
        compiler.out.mul(dest[0], src1[0], src2[0], src1[1] == Float)
        compiler.vm.push_operand(dest)

class DivOp(MulOp):
    """
    The Division Operator token.  (Is a MulOp).
    """

    def compute(self, compiler):
        """
        When called, generates a call to the __aeabi_idiv function.

        Arguments:
        compiler -- the compiler on which to act.
        """

        # Get the first source register to identify the type of the division
        src1 = compiler.vm.pop_operand()

        # Do integer difision via function call
        if src1[1] == Integer:
            src2 = compiler.vm.pop_operand()
            compiler.vm.assert_types(src1, src2)
            compiler.vm.push_operand(src1, load_tmp=True)
            compiler.vm.push_operand(src2, load_tmp=True)
            compiler.vm.call("__aeabi_idiv", args=2, rets=1)

        # Do native float division
        elif src1[1] == Float:
            compiler.vm.push_operand(src1)
            dest, src1, src2 = compiler.vm.pop_operands()
            compiler.vm.assert_types(src1, src2)
            compiler.out.div(dest[0], src2[0], src1[0], float=True)
            compiler.vm.push_operand(dest)

class AddOp(Operator):
    """
    The Addition Operator token.
    """

    _priority = 1

    def compute(self, compiler):
        """
        When called, generates an add operation in the output.

        Arguments:
        compiler -- the compiler on which to act.
        """

        dest, src1, src2 = compiler.vm.pop_operands()
        compiler.vm.assert_types(src1, src2)
        compiler.out.add(dest[0], src1[0], src2[0], dest[1] == Float)
        compiler.vm.push_operand(dest)

class SubOp(AddOp):
    """
    The Subtraction Operator token.  (Is an AddOp Token).
    """

    def compute(self, compiler):
        """
        When called, generates an subtraction operation in the output.

        Arguments:
        compiler -- the compiler on which to act.
        """

        dest, src1, src2 = compiler.vm.pop_operands()
        compiler.vm.assert_types(src1, src2)
        compiler.out.sub(dest[0], src2[0], src1[0])
        compiler.vm.push_operand(dest)

class NegOp(Operator):
    """
    The Negation Operator token.  Replaces
    """

    _priority = 2.5

    def compute(self, compiler):
        """
        When called, generates a negation operation in the output.

        Arguments:
        compiler -- the compiler on which to act.
        """

        src = compiler.vm.pop_operand()
        if src[0] == Integer:
            dst = compiler.vm.regs.get_tmp_reg()
            compiler.out.neg(dst, src, float=False)
            dst = (dst, Integer)
        elif src[0] == Float:
            dst = compiler.vm.fregs.get_tmp_reg()
            compiler.out.neg(dst, src, float=True)
            dst = (dst, Float)
        compiler.vm.push_operand(dst)

class Comparator(Operator):
    """
    The Comparator token.
    """

    _priority = 0

class LessThan(Comparator):
    """
    The Less Than token. (Is a Comparator Token).
    """

    def compute(self, compiler):
        """
        When called, generates an comparison operation in the output.

        Arguments:
        compiler -- the compiler on which to act.
        """

        rhs = compiler.vm.pop_operand()
        lhs = compiler.vm.pop_operand()
        compiler.vm.assert_types(rhs, lhs)
        compiler.out.cmp(lhs[0], rhs[0], lhs[1] == Float)
        compiler.out.branch(compiler.vm.pop_label(), "GE")

class LessThanEqual(Comparator):
    """
    The Less Than Equal token. (Is a Comparator Token).
    """

    def compute(self, compiler):
        """
        When called, generates a <= comparison in the output.

        Arguments:
        compiler -- the compiler on which to act.
        """

        rhs = compiler.vm.pop_operand()
        lhs = compiler.vm.pop_operand()
        compiler.vm.assert_types(rhs, lhs)
        compiler.out.cmp(rhs[0], lhs[0], lhs[1] == Float)
        compiler.out.branch(compiler.vm.pop_label(), "LT")

class NotEqual(Comparator):
    """
    The Not Equal token. (Is a Comparator Token).
    """

    def compute(self, compiler):
        """
        When called, generates a != comparison in the output.

        Arguments:
        compiler -- the compiler on which to act.
        """

        rhs = compiler.vm.pop_operand()
        lhs = compiler.vm.pop_operand()
        compiler.vm.assert_types(rhs, lhs)
        compiler.out.cmp(lhs[0], rhs[0], lhs[1] == Float)
        compiler.out.branch(compiler.vm.pop_label(), "EQ")

class Equal(Comparator):
    """
    The Equality token. (Is a Comparator Token).
    """

    def compute(self, compiler):
        """
        When called, generates a == comparison in the output.

        Arguments:
        compiler -- the compiler on which to act.
        """

        rhs = compiler.vm.pop_operand()
        lhs = compiler.vm.pop_operand()
        compiler.vm.assert_types(rhs, lhs)
        compiler.out.cmp(lhs[0], rhs[0], lhs[1] == Float)
        compiler.out.branch(compiler.vm.pop_label(), "NE")

class GreaterThan(Comparator):
    """
    The Greater Than token. (Is a Comparator Token).
    """

    def compute(self, compiler):
        """
        When called, generates a > comparison in the output.

        Arguments:
        compiler -- the compiler on which to act.
        """

        rhs = compiler.vm.pop_operand()
        lhs = compiler.vm.pop_operand()
        compiler.vm.assert_types(rhs, lhs)
        compiler.out.cmp(rhs[0], lhs[0], lhs[1] == Float)
        compiler.out.branch(compiler.vm.pop_label(), "GE")

class GreaterThanEqual(Comparator):
    """
    The Greater Than Equal token. (Is a Comparator Token).
    """

    def compute(self, compiler):
        """
        When called, generates a >= comparison in the output.

        Arguments:
        compiler -- the compiler on which to act.
        """

        rhs = compiler.vm.pop_operand()
        lhs = compiler.vm.pop_operand()
        compiler.vm.assert_types(rhs, lhs)
        compiler.out.cmp(lhs[0], rhs[0], lhs[1] == Float)
        compiler.out.branch(compiler.vm.pop_label(), "LT")

class Assign(Token):
    """
    The Assignment token.
    """

    pass

class StatementSep(Token):
    """
    The Statement Separator token.
    """

    pass

class LeftParen(Operator):
    """
    The Left Parenthesis token.
    """

    _priority = 4

    pass

class RightParen(Operator):
    """
    The Right Parenthesis token.
    """

    _priority = 3

    pass

class EndOfFile(Token):
    """
    The End Of File token.  Signifies that there are no more characters to be
    read in the file.
    """

    pass
