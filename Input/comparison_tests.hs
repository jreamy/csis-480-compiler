
program comparisons

define gte:integer on v1:float, v2:float as
    begin
        print v1;
        print " >= ";
        print v2;
        if v1 >= v2 then print " true\n" else print " false\n"
    end

define lte:integer on v1:float, v2:float as
    begin
        print v1;
        print " <= ";
        print v2;
        if v1 <= v2 then print " true\n" else print " false\n"
    end

define lt:integer on v1:float, v2:float as
    begin
        print v1;
        print " <  ";
        print v2;
        if v1 < v2 then print " true\n" else print " false\n"
    end

define gt:integer on v1:float, v2:float as
    begin
        print v1;
        print " >  ";
        print v2;
        if v1 > v2 then print " true\n" else print " false\n"
    end

thing:integer;
o:float

begin
    o := 5.3;
    thing := gte(o, 1.2);
    thing := gte(o, o);
    thing := gte(o, 7.4);
    thing := lte(o, 1.2);
    thing := lte(o, o);
    thing := lte(o, 7.4);
    thing := lt(o, 1.2);
    thing := lt(o, o);
    thing := lt(o, 7.4);
    thing := gt(o, 1.2);
    thing := gt(o, o);
    thing := gt(o, 7.4)
end
