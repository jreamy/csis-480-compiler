
{
   Tim Goodfellow
   Hansen Stout (Pseudo) Random Number Generator
   Adapted from http://www.christianpinder.com/articles/pseudo-random-number-generation/
}

program rand

{ Just prints our number. }
define print_value:integer on num:integer as
   begin
      print num;
      print "\n";
      print_value := 0
   end

{ Simple algorithm to generate a new number. }
define gen_pseudo_rand:integer on seed:integer as
   k1:integer;
   ix: integer
   begin
      k1 := seed / 127773;
      ix := 16807 * (seed - k1 * 127773) - k1 * 2836;
      if ix < 0 then
         ix := ix + 2147483647;
      gen_pseudo_rand := ix
   end

{ Just the variables... }
seed:integer;
rand:integer;
continue:integer;
void:integer { A variable named void. Wait, what? }

{ "Main" }
begin
   { Fetch the seed. }
   print "Enter seed number (0 to quit) : ";
   read seed;

   { Seed is the gate for our while loop to start. }
   continue := seed;

   { Just a loop... }
   while continue <> 0 do
      begin
         { Generate our random number. }
         rand := gen_pseudo_rand(seed);

         { Print our random number. This is what happens when we don't have void return types.}
         void := print_value(rand);

         { Seed our next round. }
         seed := rand;

         { Do we have a next round? }
         print "Enter any number to get the next random number or 0 to quit: ";
         read continue
      end
end
