
program guesser

   define tester:integer on value:integer, prompt:string as
    guess:integer

   begin
     print prompt;
     read guess;
     if guess = value then
       tester := 1
     else
       tester := 0
   end


   result:integer

   begin
      result := 0;

      while result <> 1 do
      begin
         result := tester(5, "hi, please guess a number: ");
         if result = 1 then
            print "Yay, you got it!\n"
         else
            print "try again!\n"
      end
   end
