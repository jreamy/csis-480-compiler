program Factorial
	define factorial : integer on num : integer as
	begin
		factorial := 1;
		while num > 0 do
		begin
			factorial := factorial * num;
			num := num - 1
		end
	end

	startNum : integer

	begin
		print "Enter an integer whose factorial shall be calculated: ";
		read startNum;

		print factorial(startNum);
		print "\n"
	end
