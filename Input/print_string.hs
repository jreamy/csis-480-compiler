
program print_string

define func:string on str:string, int:integer, flt:float as
    begin
        print "\npassed to function:";
        print "\n - int: ";
        print int;
        print "\n - float: ";
        print flt;
        print "\n - string: ";
        print str;
        func := "\nthis string was returned from the function!\n"
    end

int:integer;
str:string;
flt:float

begin
    print "input a string: ";
    read str;
    print "input an integer: ";
    read int;
    print "input a float: ";
    read flt;
    print func(str, int, flt)
end
