program maximumFinder

define max_num:float on num1:float, num2:float as
   begin
     if num1 >= num2 then
       max_num := num1
     else
       max_num := num2
   end

num1:float;
num2:float
begin
   print "Please enter two numbers, and I will tell you which one is bigger!\n";
   print "number1: ";
   read num1;
   print "number2: ";
   read num2;

   print max_num(num1, num2);
   print " is the bigger number!\n"
end
