
program fibo

define fibonacci:integer on num:integer as

    begin
        if num <= 1 then fibonacci := 1
        else fibonacci := fibonacci(num-1) + fibonacci(num-2)
    end


num:integer;
s1:string;
s2:string

begin
    print "enter int: ";
    read num;
    print "fibonacci is: ";
    print fibonacci(num);
    print "\n";
    print "fibonacci of ";
    print (fibonacci(num) + 1);
    print " is ";
    print fibonacci(1 + fibonacci(num));
    print "\n"

end
