
# CSIS 480 - HansenStout Compiler

This project is the compiler I designed for the [HansenStout Grammar](HansenStout Grammar.pdf) provided in the CSIS 480 course.  

### The Compiler:
 - [Lexical Analyzer](Compiler/lexical_analysis.py#L258) that parses the lexical tokens of an input file
 - [Syntax Directed Compiler](Compiler/compiler.py#L14) that directs the translation of the input stream of tokens into an output in ARMv7
 - [Virtual Machine](Compiler/virtual_machine.py#L22) that handles translating the abstract pushing and popping of operands and operators in the internal computation stacks into register allocation and assembly instructions
 - [Register Allocator](Compiler/register_allocator.py#L7) that handles the concrete freeing and loading of registers.  Has a 'virtual stack' for computation that consists of a few registers and the actual stack, and separate set of variable registers
 - [Symbols](Compiler/symbols.py), [Actions](Compiler/actions.py#L9), [Tokens](Compiler/tokens.py#L131) that are used by the compiler during parse
 - [ARMv7 Architecture](Compiler/architectures.py#L5) the only currently supported backend is the ARMv7, but allows easy extension into other assembly languages

 ### The HansenLite Grammar:
 The [HansenStout Grammar](HansenStout Grammar.pdf) is a mix of C and pigeon Pascal that supports function declarations and both int and float datatypes.  A collection of sample [input files](Input) can be found along with their [outputs](Output) in ARMv7 assembly.
